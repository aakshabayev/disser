<?php
// prevent the server from timing out
set_time_limit(0);

// include the web sockets server script (the server is started at the far bottom of this file)
require 'class.PHPWebSocket.php';

// when a client sends data to the server
function wsOnMessage($clientID, $message, $messageLength, $binary) {
	global $Server;
	$Server->log($message);

	$ip = long2ip( $Server->wsClients[$clientID][6] );

	// check if message length is 0
	if ($messageLength == 0) {
		$Server->wsClose($clientID);
		return;
	}

	$arr = explode(",", $message);
	// getting page
	$tmp = explode(":", $arr[0]);
	$page = $tmp[1];

	// getting activity id
	$tmp = explode(":", $arr[1]);
	$activity_id = $tmp[1];

	// getting group id
	$tmp = explode(":", $arr[2]);
	$group_id = $tmp[1];

	// getting message
	$tmp = explode(":", $arr[3]);
	$msg = $tmp[1];

	if ($page == 'multiple') {
		$tmp = explode(":", $arr[4]);
		$question_id = $tmp[1];
	}

	// if page == competition, save additional data
	if ($page == "competitionanswer") {
		$tmp = explode(":", $arr[4]);
		$user_id = $tmp[1];

		$tmp = explode(":", $arr[5]);
		$type = $tmp[1];

		$tmp = explode(":", $arr[6]);
		$question_id = $tmp[1];


		$servername = "localhost";
		$username = "root";
		$password = "";
		$dbname = "disser";

		// Create connection
		$conn = new mysqli($servername, $username, $password, $dbname);
		// Check connection
		if ($conn->connect_error) {
		    die("Connection failed: " . $conn->connect_error);
		}

		$sql = "SELECT * FROM question_competition where id=$question_id";
		$result = $conn->query($sql);

		if ($result->num_rows > 0) {
		    // output data of each row
		    while($row = $result->fetch_assoc()) {
		        $answer1 = $row['answer1'];
		        $answer2 = $row['answer1'];
		        $answer3 = $row['answer1'];
		        $answer4 = $row['answer1'];
		        $answer5 = $row['answer1'];
		        $answer_correct = $row['answer_correct'];

				$correct = 0;
				if ($type == 'multiple' && (string)($answer_correct) == $msg) {
					$correct = 1;
				}

				if ($type == 'yesno' && (string)($answer_correct) == $msg) {
					$correct = 1;
				}

				if ($type == 'open') {
					$str = $msg;
					$str=preg_replace('/\s+/', '', $str);
					$str = strtolower($str);

					$str1 = $answer1;
					$str1=preg_replace('/\s+/', '', $str1);
					$str1 = strtolower($str1);

					$str2 = $answer2;
					$str2=preg_replace('/\s+/', '', $str2);
					$str2 = strtolower($str2);

					$str3 = $answer3;
					$str3=preg_replace('/\s+/', '', $str3);
					$str3 = strtolower($str3);

					$str4 = $answer4;
					$str4=preg_replace('/\s+/', '', $str4);
					$str4 = strtolower($str4);

					$str5 = $answer5;
					$str5=preg_replace('/\s+/', '', $str5);
					$str5 = strtolower($str5);

					if ($str == $str1 || $str == $str2 || $str == $str3 || $str == $str4 || $str == $str5)
						$correct = 1;
				}
				foreach ( $Server->wsClients as $id => $client ) {
					if ( $id != $clientID and $Server->wsClients[$clientID]['page'] == 'competition' and $Server->wsClients[$clientID]['group'] == $client['group']) {
						$Server->wsSend($id, $user_id . ":" . (string)$correct);
					}
				}

		    }
		} else {
		}
		$conn->close();
	}




	/////////////////////////////////////////////

	if ($msg == "start") {
		$Server->wsClients[$clientID]['page'] = $page; // 12
		$Server->wsClients[$clientID]['activity'] = $activity_id; // 13
		$Server->wsClients[$clientID]['group'] = $group_id; // 14
		if ($page == "multiple") {
			$Server->wsClients[$clientID]['question_id'] = $question_id; // 14
		}
	} else {
		$Server->log($question_id);
		//The speaker is the only person in the room. Don't let them feel lonely.
		if ( sizeof($Server->wsClients) == 1 ) {
			//$Server->wsSend($clientID, "you are alone at this time");
		}
		else if ($page != 'competitionanswer') {
			//Send the message to everyone but the person who said it
			foreach ( $Server->wsClients as $id => $client ) {
				if ( $id != $clientID and $Server->wsClients[$clientID]['page'] == $client['page'] and $Server->wsClients[$clientID]['group'] == $client['group']) {
					if ($page == 'multiple') {
						if ($question_id == $client['question_id'])
							$Server->wsSend($id, $msg);
					}
					else
						$Server->wsSend($id, $msg);

				}
			}
		}
	}
}

// when a client connects
function wsOnOpen($clientID)
{
	global $Server;
	$ip = long2ip( $Server->wsClients[$clientID][6] );

	//$Server->log( "$ip ($clientID) has connected." );

	//Send a join notice to everyone but the person who joined
	foreach ( $Server->wsClients as $id => $client ) {
		if ( $id != $clientID) {
			//$Server->wsSend($id, "Visitor $clientID ($ip) has joined the room.");
		}

	}
}

// when a client closes or lost connection
function wsOnClose($clientID, $status) {
	global $Server;
	$ip = long2ip( $Server->wsClients[$clientID][6] );

	//$Server->log( "$ip ($clientID) has disconnected." );

	//Send a user left notice to everyone in the room
	// foreach ( $Server->wsClients as $id => $client )
	// 	$Server->wsSend($id, "Visitor $clientID ($ip) has left the room.");
}

// start the server
$Server = new PHPWebSocket();
$Server->bind('message', 'wsOnMessage');
$Server->bind('open', 'wsOnOpen');
$Server->bind('close', 'wsOnClose');
// for other computers to connect, you will probably need to change this to your LAN IP or external IP,
// alternatively use: gethostbyaddr(gethostbyname($_SERVER['SERVER_NAME']))
$Server->wsStartServer('46.101.26.127', 9300);
// $Server->wsStartServer('127.0.0.1', 9300);

?>