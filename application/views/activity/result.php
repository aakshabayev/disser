<div id="container">
    <h1>
       	<!-- <font color="red"><b>Dear, <?php $user = Auth::instance()->get_user()->username; echo($user)?>!</font></b> -->
        Group number: #<font color="red"><?=$group->id?></font><br><br>
        <font color="red"><b><?=$activity->question?></font></b>
    </h1>
    <div>
        <h1>
            <div id='total'>
        	   Total votes: <?=$total_yes+$total_no?> Yes: <?=$total_yes?> No: <?=$total_no?>
            </div>
        </h1>
        <h2>



        <?php

        $k=100;
        $total_yes_per=0;
        $total_no_per=0;

        $n = $total_yes + $total_no;

        if ($total_yes>0 || $total_no>0){
            $total_yes_per = ($total_yes*100)/$n;
            $total_no_per = ($total_no*100)/$n;
        }

        ?>

        <style>
        	#rectangleyes {
				width: <?=$total_yes_per?>%;
				height: 50px;
				background: #000066;
        border-radius:20px;
			}
			#rectangleno {
				width: <?=$total_no_per?>%;
				height: 50px;
				background: #000066;
        border-radius:20px;
			}
        </style>

      <?php
      if ($total_yes>0) {
      ?>
        <font color="yellow"><p id="rectangleyes">YES: <?=round($total_yes_per,0)?>%</p></font>
        <?php
      }else{
        ?>
        <font color="black"><p id="rectangleyes">YES: <?=$total_yes_per?>%</p></font>
      <?php
      }
      ?>

      <br>

      <?php
      if ($total_no>0) {
      ?>
        <font color="yellow"><p id="rectangleno">NO: <?=round($total_no_per,0)?>%</p></font>
      <?php
      }else{
        ?>
        <font color="black"><p id="rectangleno">NO: <?=$total_no_per?>%</p></font>
      <?php
      }
      ?>





		<!-- <progress value="<?=$total_yes_per?>" max="100">YES: <?=$total_yes_per?>%</progress>
			<br>
		<progress value="<?=$total_no_per?>" max="100">YES: <?=$total_no_per?>%</progress>
		 -->
		</h2>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript" src="/public/js/my_websocket.js"></script>

  <script>
    var activity_id = "<?=$activity->activity_id?>"
    var group_id = "<?=$group->id?>";

    var Server;

    function send( text ) {
      Server.send( 'message', text );
    }

    $(document).ready(function() {
      Server = new MyWebSocket('ws://46.101.26.127:9300');
      // Server = new MyWebSocket('ws://127.0.0.1:9300');
      //Let the user know we're connected
      Server.bind('open', function() {
        send("page:yesno,activity_id:" + activity_id + ",group:" + group_id + ",message:start");
        console.log( "Connected." );
      });

      //OH NOES! Disconnection occurred.
      Server.bind('close', function( data ) {
        console.log( "Disconnected." );
      });
      var total_yes = <?=$total_yes?>;
      var total_no = <?=$total_no?>;
      //Log any messages sent from server
      Server.bind('message', function( payload ) {
        console.log(payload);
        if (payload.indexOf("YES_ANSWER_" + activity_id) > -1) {
            total_yes = total_yes + 1;
        }
        if (payload.indexOf("NO_ANSWER_" + activity_id) > -1) {
            total_no = total_no + 1;
        }
        k=100;
        total_yes_per=0;
        total_no_per=0;

        n = total_yes + total_no;

        total_yes_per = (total_yes*100)/n;
        total_no_per = (total_no*100)/n;
        $("#rectangleyes").html("YES: " + Math.round(total_yes_per,0) + "%");
        $("#rectangleyes").css('color', 'yellow');
        $("#rectangleyes").width(total_yes_per + "%");
        $("#rectangleno").html("NO: " + Math.round(total_no_per,0) + "%");
        $("#rectangleno").css('color', 'yellow');
        $("#rectangleno").width(total_no_per + "%");
        $("#total").html("Total votes: " +  n + " Yes: " + total_yes + " No: " + total_no);

      });
      //

      Server.connect();
    });
  </script>

