<div id="container" >
   	<h1>
       	<b>Create a quick(YES/NO) activity</b>
    </h1>
  	<form action="/activity/create" method="post" autocomplete="off" class="validate-form">
      <div align="center">
        <table frame="box" class ="create_activity_yesno">
            <tr>
                <td class="reg_td">
                    <p align="center"><b>Activity name</b></p>
                </td>
                <td class="reg_td"> 
                    <input name="name"  type="text" style="background:yellow;" placeholder="Activity name" title="" required>
                </td>
            </tr>
            <tr>
                <td class="reg_td">
                    <p align="center"><b>Question for YES, NO answer</b></p>
                </td class="reg_td">
                <td class="reg_td">
                    <input name="question"  type="text"  placeholder="Question" title="" required>
                </td>
            </tr>
        </table>
        <br>
        <p align="center"><input type="submit" name="submit_activity" value="Create" class="btn"></p> 
      </div>
    </form>
</div>

