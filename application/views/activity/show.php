<!-- /*
  Based on PHP WebSocket Server 0.2
   - http://code.google.com/p/php-websocket-server/
   - http://code.google.com/p/php-websocket-server/wiki/Scripting

  WebSocket Protocol 07
   - http://tools.ietf.org/html/draft-ietf-hybi-thewebsocketprotocol-07
   - Supported by Firefox 6 (30/08/2011)

  Whilst a big effort is made to follow the protocol documentation, the current script version may unknowingly differ.
  Please report any bugs you may find, all feedback and questions are welcome!
*/
 -->
  <style>
    input, textarea {border:1px solid #CCC;margin:0px;padding:0px}
    #body {max-width:800px;margin:auto}
    #log {width:100%;height:400px}
    #message {width:100%;line-height:20px}
  </style>


<div id="container" >

          <h1>Group number: #<font color="red"><?=$group->id?></font><br><br>
          <font color="red"><?=$activity->question?></font></h1>
          <form action="/activity/yesno" method="post" autocomplete="off">
         <!--  <input type="radio" name="yesno" value="1" checked>Yes
          <br>
          <input type="radio" name="yesno" value="0">No
          <br> -->

          <!-- <input type="submit" name="yesno" value="1" class="btn"> -->
          <div class="next-div">
            <button id='yes' name="yesno" type="submit" value="1" class="round-button">YES</button>
          </div>

          <div class="next-diva">
            <button id='no' name="yesno" type="submit" value="0" class="round-button">NO</button>
          </div>


          <!-- <input type="submit" name="yesno" value="0" class="btn"> -->

          <!-- <input type="submit" name="submit_vote" value="Vote" class="btn">
          <br> -->
          <input type='hidden' value='<?=$activity->activity_id?>' name='activity_id'>
       	</form>

</div>

<textarea id='log' name='log' readonly='readonly'></textarea><br/>
<input type='text' id='message' name='message' />


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript" src="/public/js/countdown.js"></script>
<script type="text/javascript" src="/public/js/my_websocket.js"></script>

  <script>
    var activity_id = "<?=$activity->activity_id?>";
    var group_id = "<?=$group->id?>";
    var Server;

    function log( text ) {
      $log = $('#log');
      //Add text to log
      $log.append(($log.val()?"\n":'')+text);
      //Autoscroll
      $log[0].scrollTop = $log[0].scrollHeight - $log[0].clientHeight;
    }

    function send( text ) {
      Server.send( 'message', text );
    }

    $(document).ready(function() {
      log('Connecting...');
      Server = new MyWebSocket('ws://46.101.26.127:9300');
      // Server = new MyWebSocket('ws://127.0.0.1:9300');

      $('#message').keypress(function(e) {
        if ( e.keyCode == 13 && this.value ) {
          log( 'You: ' + this.value );
          send( this.value );

          $(this).val('');
        }
      });

      $('#yes').click(function(e) {
          send("page:yesno,activity_id:" + activity_id + ",group:" + group_id + ",message:" + 'YES_ANSWER_' + activity_id);
      });

      $('#no').click(function(e) {
          send("page:yesno,activity_id:" + activity_id + ",group:" + group_id + ",message:" + 'NO_ANSWER_' + activity_id);
      });

      //Let the user know we're connected
      Server.bind('open', function() {
        send("page:yesno,activity_id:" + activity_id + ",group:" + group_id + ",message:start");
        log( "Connected." );
      });

      //OH NOES! Disconnection occurred.
      Server.bind('close', function( data ) {
        log( "Disconnected." );
      });

      //Log any messages sent from server
      Server.bind('message', function( payload ) {
        log( payload );
      });

      Server.connect();
    });
  </script>
