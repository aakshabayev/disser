
<div id="container">
    <h1>
       	<font color="red"><b>Dear <?php $user = Auth::instance()->get_user()->username; echo($user)?>, you have created next activities:</font></b>
    </h1>
    <div>
        <h1>
        	<b><u>Quick feedback:</u></b>
        </h1>
        <h2>
        <?php $k=0; foreach ($activities as $activity) { $k=$k+1; ?>
			<p align="left"><font><?=$k?>. <a href="/activity/result?id=<?=$activity->activity_id?>"> <?=$activity->name?> </a> / Group number: #<?=$activity->gid?>/ Date-time: <?=$activity->date?></font><p>
			<br>
		<?php } ?>
		</h2>
    </div>
    <div>
        <h1>
            <b><u>Multiple votes:</u></b>
        </h1>
        <h2>
        <?php $k=0; foreach ($activities_multiple as $activity) { $k=$k+1; ?>
            <p align="left"><font><?=$k?>. <a href="/activity/resultmultiple?id=<?=$activity->id?>&order=1"> <?=$activity->name?> </a> / Group number: #<?=$activity->gid?> / Date-time: <?=$activity->date?></font><p>
            <br>
        <?php } ?>
        </h2>
        <h1>
            <b><u>Competition activities:</u></b>
        </h1>
        <h2>
        <?php $k=0; foreach ($activity_competition as $activity) { $k=$k+1; ?>
            <p align="left"><font><?=$k?>. <a href="/activity/showcompetition?id=<?=$activity->id?>&order=1"> <?=$activity->name?> </a> / Group number: #<?=$activity->gid?> / Date-time: <?=$activity->date?></font><p>
            <br>
        <?php } ?>
        </h2>
        <h1>
            <b><u>Participatory drawing activities:</u></b>
        </h1>
        <h2>
        <?php $k=0; foreach ($activity_drawing as $activity) { $k=$k+1; ?>
            <p align="left"><font><?=$k?>. <a href="/activity/drawing?id=<?=$activity->id?>"> <?=$activity->name?> </a> / Group number: #<?=$activity->gid?> / Date-time: <?=$activity->date?></font><p>
            <br>
        <?php } ?>
        </h2>
    </div>
</div>
