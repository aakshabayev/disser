<div id="container" >
    <h1>
         Registration page
    </h1>
    <div align="center">
         <form action="/user/register" method="post" autocomplete="off" class="validate-form">
                <input type='hidden' name='role' value='login'>
                <table class = "create_activity" frame="box">
                    <tr>
                        <td class ="reg_td"><a href="#openModal">Login* </a></td>
                        <div id="openModal" class="modalDialog">
                            <div>
                                <a href="#close" title="Close" class="close">x</a>
                                <h2>What is login?</h2>
                                <p>Login <b>must</b> contain both <b>first name</b> and <b>second name</b>
                                separated by <b>dot</b>. For example: if your first name is John and Second name is Smith your login must be <b>John.Smith</b></p>
                            </div>
                        </div>
                        <td class ="reg_td"><input name="username"  type="text"  placeholder="name.surname" style="background: yellow;" title="Login cannot be empty" required></td>
                    </tr>
                    <tr>
                        <td class="reg_td">Email</td>
                        <td class ="reg_td"><input name="email" type="email" placeholder="abcde@abc.de" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" title="Please enter correct email address" required></td>

                    </tr>

                    <tr>
                        <td class="reg_td">Password</td>
                        <td class ="reg_td"><input type="password" name="password" pattern=".{4,}" title="Password must be more than 4 characters" placeholder="********" class = "colortext" onchange="form.cpw.pattern = this.value;" required></td>
                    </tr>

                    <tr>
                        <td class="reg_td">Confirm password</td>
                        <td class ="reg_td"><input type="password" name="cpw" pattern=".{4,}" title="Password is not the same" placeholder="********" class = "colortext" required></td>
                    </tr>


                </table>
                <div style="padding-top: 30px;">
                <p>
                    <input type="submit" name="submit_registration" value="REGISTRATION" class="btn">
                </p>    
                </div>
                

            </form>
    </div>    
</div>
