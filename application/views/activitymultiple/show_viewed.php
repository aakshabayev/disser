<!--<?=$question_multiple->question?><br>
<?=$question_multiple->answer1?>  =  <?=$answer1?><br>
<?=$question_multiple->answer2?>  =  <?=$answer2?><br>
<?=$question_multiple->answer3?>  =  <?=$answer3?><br> -->

<div id="container">
    <h1>
       	<!-- <font color="red"><b>Dear, <?php $user = Auth::instance()->get_user()->username; echo($user)?>!</font></b> -->
       	Group number: #<font color="red"><?=$group->id?></font><br>
        <font color="red"><b><?=$question_multiple->question?></b></font><br>
        <div id="total">Total votes: <?=$answer1+$answer2+$answer3?><!-- . Answer#1: <?=$answer1?>. Answer#2: <?=$answer2?>. Answer#3: <?=$answer3?>.  --></div>
    </h1>

    <div>

        <h2>

        <?=$activity->question?>

        <?php

        $k=100;
        $answer_one_per=0;
        $answer_two_per=0;
        $answer_three_per=0;

        if ($answer1>0 || $answer2>0 || $answer3>0){
            $answer_one_per = ($answer1/($answer1+$answer2+$answer3))*100;
            $answer_two_per = (($answer2)/($answer1+$answer2+$answer3))*100;
            $answer_three_per = (($answer3)/($answer1+$answer2+$answer3))*100;
        }
        ?>

        <style>
        	#rectangleanswer1 {
				width: <?=$answer_one_per?>%;
				height: 50px;
				background: #000066;
                border-radius:20px;
			}
			#rectangleanswer2 {
				width: <?=$answer_two_per?>%;
				height: 50px;
				background: #000066;
                border-radius:20px;
			}
			#rectangleanswer3 {
				width: <?=$answer_three_per?>%;
				height: 50px;
				background: #000066;
                border-radius:20px;
			}

        </style>

        <?php
        if ($answer1>0) {
        ?>
			<p id="rectangleanswer1"><font color="yellow"><?=$question_multiple->answer1?>: <?=round($answer_one_per,0)?>%</font></p>
        <?php
        }else{
        ?>
			<p id="rectangleanswer1"><font color="black"><?=$question_multiple->answer1?>: <?=$answer_one_per?>%</font></p>
        <?php
        }
        ?>
		<br>


		<?php
        if ($answer2>0) {
        ?>
			<p id="rectangleanswer2"><font color="yellow"><?=$question_multiple->answer2?>: <?=round($answer_two_per,0)?>%</font></p>
		<?php
        }else{
        ?>
        	<p id="rectangleanswer2"><font color="black"><?=$question_multiple->answer2?>: <?=$answer_two_per?>%</font></p>
		<?php
        }
        ?>
		<br>

		<?php
        if ($answer3>0) {
        ?>
			<p id="rectangleanswer3"><font color="yellow"><?=$question_multiple->answer3?>: <?=round($answer_three_per,0)?>%</font></p>
		<?php
        }else{
        ?>
        	<p id="rectangleanswer3"><font color="black"><?=$question_multiple->answer3?>: <?=$answer_three_per?>%</font></p>
        <?php
        }
        ?>
		<!-- <progress value="<?=$total_yes_per?>" max="100">YES: <?=$total_yes_per?>%</progress>
			<br>
		<progress value="<?=$total_no_per?>" max="100">YES: <?=$total_no_per?>%</progress>
		 -->
		<br><br>
            Question vote:
            <?php
            for ($i = 1; $i <= $question_count; $i++) {
            ?>
                <a href="/activity/showmultiple?id=<?=$question_multiple->activity_multiple_id?>&order=<?=$i?>"><?=$i?> | </a>
            <?php
            }
            ?>
            <br><br>
            Question result:
            <?php
            for ($i = 1; $i <= $question_count; $i++) {
            ?>
                <a href="/activity/resultmultiple?id=<?=$question_multiple->activity_multiple_id?>&order=<?=$i?>"><?=$i?> | </a>
            <?php
            }
            ?>
		</h2>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript" src="/public/js/my_websocket.js"></script>

  <script>
    var activity_id = "<?=$$question_multiple->activity_multiple_id?>"
    var group_id = "<?=$group->id?>";
    var question_id = "<?=$question_multiple->id?>";

    var Server;

    function send( text ) {
      Server.send( 'message', text );
    }

    $(document).ready(function() {
      Server = new MyWebSocket('ws://46.101.26.127:9300');
      // Server = new MyWebSocket('ws://127.0.0.1:9300');
      //Let the user know we're connected
      Server.bind('open', function() {
        send("page:multiple,activity_id:" + activity_id + ",group:" + group_id + ",message:start" + ",question_id:" + question_id);
        console.log( "Connected." );
      });

      //OH NOES! Disconnection occurred.
      Server.bind('close', function( data ) {
        console.log( "Disconnected." );
      });
        var k = 100;
        var answer_one_per=0;
        var answer_two_per=0;
        var answer_three_per=0;
        var answer1 = <?=$answer1?>;
        var answer2 = <?=$answer2?>;
        var answer3 = <?=$answer3?>;

        var answer_one_per = 0;
        var answer_two_per = 0;
        var answer_three_per = 0;

      //Log any messages sent from server
      Server.bind('message', function( payload ) {
        console.log(payload);
        if (payload.indexOf("answer1_" + activity_id) > -1) {
            answer1 = answer1 + 1;
        }
        if (payload.indexOf("answer2_" + activity_id) > -1) {
            answer2 = answer2 + 1;
        }
        if (payload.indexOf("answer3_" + activity_id) > -1) {
            answer3 = answer3 + 1;
        }
        answer_one_per = (answer1/(answer1+answer2+answer3))*100;
        answer_two_per = ((answer2)/(answer1+answer2+answer3))*100;
        answer_three_per = ((answer3)/(answer1+answer2+answer3))*100;

        $("#rectangleanswer1").html("<?=$question_multiple->answer1?>" + ": " + Math.round(answer_one_per,0) + "%");
        $("#total").html("total votes: " + (answer1+answer2+answer3));
        if (answer_one_per>0) {
          $("#rectangleanswer1").css('color', 'yellow');  
        };
        $("#rectangleanswer1").width(answer_one_per + "%");

        $("#rectangleanswer2").html("<?=$question_multiple->answer2?>" + ": " + Math.round(answer_two_per,0) + "%");
        if (answer_two_per>0) {
          $("#rectangleanswer2").css('color', 'yellow');
        };
        $("#rectangleanswer2").width(answer_two_per + "%");

        $("#rectangleanswer3").html("<?=$question_multiple->answer3?>" + ": " + Math.round(answer_three_per,0) + "%");
        if (answer_three_per>0) {
          $("#rectangleanswer3").css('color', 'yellow');
        };
        $("#rectangleanswer3").width(answer_three_per + "%");

      });

      Server.connect();
    });
  </script>



<!--
  <script>
    var activity_id = "<?=$activity->activity_id?>"
    var Server;

    function send( text ) {
      Server.send( 'message', text );
    }

    $(document).ready(function() {
      Server = new MyWebSocket('ws://46.101.26.127:9300');
      //Let the user know we're connected
      Server.bind('open', function() {
        console.log( "Connected." );
      });

      //OH NOES! Disconnection occurred.
      Server.bind('close', function( data ) {
        console.log( "Disconnected." );
      });
      var total_yes = <?=$total_yes?>;
      var total_no = <?=$total_no?>;
      //Log any messages sent from server
      Server.bind('message', function( payload ) {
        console.log(payload);
        if (payload.indexOf("YES_ANSWER_" + activity_id) > -1) {
            total_yes = total_yes + 1;
        }
        if (payload.indexOf("NO_ANSWER_" + activity_id) > -1) {
            total_no = total_no + 1;
        }
        k=100;
        total_yes_per=0;
        total_no_per=0;

        n = total_yes + total_no;

        total_yes_per = (total_yes*100)/n;
        total_no_per = (total_no*100)/n;
        $("#rectangleyes").html("YES:" + total_yes_per + "%");
        $("#rectangleyes").width(total_yes_per + "%");
        $("#rectangleno").html("NO:" + total_no_per + "%");
        $("#rectangleno").width(total_no_per + "%");
        $("#total").html("Total votes:" +  n + "Yes: " + total_yes + " No: " + total_no);

      });

      Server.connect();
    });
  </script>
 -->
