<!--

<?=$activity_multiple->name?><br>

<?=$question_multiple->question?><br>
<?=$question_multiple->answer1?><br>
<?=$question_multiple->answer2?><br>
<?=$question_multiple->answer3?><br>


 <form method="post" action='/activity/answermultiple'>
    <input type='hidden' name='answer' id='answer'>
    <input type='hidden' name='question_id' value='<?=$question_multiple->id?>'>
    <input type='hidden' name='activity_id' value='<?=$activity_multiple->id?>'>
    <input type='hidden' name='order' value='<?=$question_multiple->order?>'>
    <input type='submit' id='button1' value="1">
    <input type='submit' id='button2' value="2">
    <input type='submit' id='button3' value="3">
</form>
 -->


<script src='//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

<div id="container">
    <h1>
        <!-- <font color="red"><b>Dear, <?php $user = Auth::instance()->get_user()->username; echo($user)?>!</font></b> -->
        Group number: #<font color="red"><?=$group->id?></font><br>
        <font color="red"><b><?=$question_multiple->question?></b></font>

    </h1>
    <div>
        <h2>

            #1 : <?=$question_multiple->answer1?><br><br>
            #2 : <?=$question_multiple->answer2?><br><br>
            #3 : <?=$question_multiple->answer3?><br><br>

            <form method="post" action='/activity/answermultiple'>
                <input type='hidden' name='answer' id='answer'>
                <input type='hidden' name='question_id' value='<?=$question_multiple->id?>'>
                <input type='hidden' name='activity_id' value='<?=$activity_multiple->id?>'>
                <input type='hidden' name='order' value='<?=$question_multiple->order?>'>
                <input type='submit' id='button1' value="1" class="btn">
                <input type='submit' id='button2' value="2" class="btn">
                <input type='submit' id='button3' value="3" class="btn">
            </form>
            <br><br>
            Question vote:
            <?php
            for ($i = 1; $i <= $question_count; $i++) {
            ?>
                <a href="/activity/showmultiple?id=<?=$question_multiple->activity_multiple_id?>&order=<?=$i?>"><?=$i?> | </a>
            <?php
            }
            ?>
            <br><br>
            Question result:
            <?php
            for ($i = 1; $i <= $question_count; $i++) {
            ?>
                <a href="/activity/resultmultiple?id=<?=$question_multiple->activity_multiple_id?>&order=<?=$i?>"><?=$i?> | </a>
            <?php
            }
            ?>

        </h2>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript" src="/public/js/countdown.js"></script>
<script type="text/javascript" src="/public/js/my_websocket.js"></script>

  <script>
    var activity_id = "<?=$activity_multiple->id?>";
    var group_id = "<?=$group->id?>";
    var question_id = "<?=$question_multiple->id?>";
    var Server;

    function send( text ) {
      Server.send( 'message', text );
    }

    $(document).ready(function() {
      console.log('Connecting...');
      Server = new MyWebSocket('ws://46.101.26.127:9300');
      // Server = new MyWebSocket('ws://127.0.0.1:9300');

        $('#button1').click(function() {
            $('#answer').val("1");
            send("page:multiple,activity_id:" + activity_id + ",group:" + group_id + ",message:" + 'answer1_' + activity_id + ",question_id:" + question_id);
        });
        $('#button2').click(function() {
            $('#answer').val("2");
            send("page:multiple,activity_id:" + activity_id + ",group:" + group_id + ",message:" + 'answer2_'  + activity_id + ",question_id:" + question_id);
        });
        $('#button3').click(function() {
            $('#answer').val("3");
            send("page:multiple,activity_id:" + activity_id + ",group:" + group_id + ",message:" + 'answer3_' +  activity_id + ",question_id:" + question_id);
        });

      //Let the user know we're connected
      Server.bind('open', function() {
        send("page:multiple,activity_id:" + activity_id + ",group:" + group_id + ",message:start" + ",question_id:" + question_id);
        console.log( "Connected." );
      });

      //OH NOES! Disconnection occurred.
      Server.bind('close', function( data ) {
        console.log( "Disconnected." );
      });

      //Log any messages sent from server
      Server.bind('message', function( payload ) {
        console.log( payload );
      });

      Server.connect();
    });
  </script>
