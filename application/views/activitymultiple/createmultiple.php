<div id="container" >
   	<h1>
       	<b>Create multiple votes activity</b>
    </h1>
  	<form method="post" autocomplete="off" class="validate-form">
      <div>
        <table frame="box" class="ind_table_td1" id="question_type_table">
            <tr>
                <td>
                    <p align="center"><font color="red"><b>Activity name</b></font></p>
                </td>
                <td>
                    <input name="activity_name" style="background:yellow;" type="text"  placeholder="Activity name" title="" required>
                </td>
            </tr>
            <tr>
                <td>
                    <p align="center"><b>Question #1</b></p>
                </td>
                <td>
                    <input name="question_1"  type="text"  placeholder="Question_1" title="" required>
                </td>
            </tr>

            <tr>
                <td>
                    Answer #1_1
                </td> 
                <td>
                    <input name="answer_1_1"  type="text"  placeholder="Answer_1_1" title="" required>
                </td>
            </tr>

            <tr>
                <td>
                    Answer #1_2
                </td>
                <td>
                    <input name="answer_1_2"  type="text"  placeholder="Answer_1_2" title="" required>
                </td>
            </tr>

            <tr>
                <td>
                    Answer #1_3
                </td>
                <td>
                    <input name="answer_1_3"  type="text"  placeholder="Answer_1_3" title="" required>
                </td>
            </tr>



        </table>
        <button type="button" id="add_more_question" onclick="addQuestion()" class="btn">Add more question</button>
        <button type="button" id="del_more_question" onclick="delQuestion()" class="btn" style="background: #000;" disabled>del</button>
        <input type="submit" name="submit_activity" value="Complete" class="btn">
      </div>
    </form>
</div>
<script type="text/javascript" src="/public/js/createmultiple.js"></script>


