<!doctype html>

<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title></title>
  <link rel="stylesheet" type="text/css" href="/public/css/canvas_style.css">
<script src='//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>  
  
</head>

<body>
  <div id="SketchTools">
    <!-- Basic tools -->
    <a href="#SketchPad" data-color="#000000" title="Black"><img src="/public/img/black_icon.png" alt="Black"/></a>
    <a href="#SketchPad" data-color="#ff0000" title="Red"><img src="/public/img/red_icon.png" alt="Red"/></a>
    <a href="#SketchPad" data-color="#00ff00" title="Green"><img src="/public/img/green_icon.png" alt="Green"/></a>
    <a href="#SketchPad" data-color="#0000ff" title="Blue"><img src="/public/img/blue_icon.png" alt="Blue"/></a>
    <a href="#SketchPad" data-color="#ffff00" title="Yellow"><img src="/public/img/yellow_icon.png" alt="Yellow"/></a>
    <a href="#SketchPad" data-color="#00ffff" title="Cyan"><img src="/public/img/cyan_icon.png" alt="Cyan"/></a>
    <br/>
    <!-- Advanced colors -->
    <a href="#SketchPad" data-color="#e74c3c" title="Alizarin"><img src="/public/img/alizarin_icon.png" alt="Alizarin"/></a>
    <a href="#SketchPad" data-color="#c0392b" title="Pomegrante"><img src="/public/img/pomegrante_icon.png" alt="Pomegrante"/></a>
    <a href="#SketchPad" data-color="#2ecc71" title="Emerald"><img src="/public/img/emerald_icon.png" alt="Emerald"/></a>
    <a href="#SketchPad" data-color="#1abc9c" title="Torquoise"><img src="/public/img/torquoise_icon.png" alt="Torquoise"/></a>
    <a href="#SketchPad" data-color="#3498db" title="Peter River"><img src="/public/img/peterriver_icon.png" alt="Peter River"/></a>
    <a href="#SketchPad" data-color="#9b59b6" title="Amethyst"><img src="/public/img/amethyst_icon.png" alt="Amethyst"/></a>
    <a href="#SketchPad" data-color="#f1c40f" title="Sun Flower"><img src="/public/img/sunflower_icon.png" alt="Sun Flower"/></a>
    <a href="#SketchPad" data-color="#f39c12" title="Orange"><img src="/public/img/orange_icon.png" alt="Orange"/></a>
    <br/>
    <a href="#SketchPad" data-color="#ecf0f1" title="Clouds"><img src="/public/img/clouds_icon.png" alt="Clouds"/></a>
    <a href="#SketchPad" data-color="#bdc3c7" title="Silver"><img src="/public/img/silver_icon.png" alt="Silver"/></a>
    <a href="#SketchPad" data-color="#7f8c8d" title="Asbestos"><img src="/public/img/asbestos_icon.png" alt="Asbestos"/></a>
    <a href="#SketchPad" data-color="#34495e" title="Wet Asphalt"><img src="/public/img/wetasphalt_icon.png" alt="Wet Asphalt"/></a>
    <a href="#SketchPad" data-color="#ffffff" title="Eraser"><img src="/public/img/eraser_icon.png" alt="Eraser"/></a>
    <br/>
    <!-- Size options -->
    <a href="#SketchPad" data-size="1"><img src="/public/img/pencil_icon.png" alt="Pencil"/></a>
    <a href="#SketchPad" data-size="3"><img src="/public/img/pen_icon.png" alt="Pen"/></a>
    <a href="#SketchPad" data-size="5"><img src="/public/img/stick_icon.png" alt="Stick"/></a>
    <a href="#SketchPad" data-size="9"><img src="/public/img/smallbrush_icon.png" alt="Small brush"/></a>
    <a href="#SketchPad" data-size="15"><img src="/public/img/mediumbrush_icon.png" alt="Medium brush"/></a>
    <a href="#SketchPad" data-size="30"><img src="/public/img/bigbrush_icon.png" alt="Big brush"/></a>
    <a href="#SketchPad" data-size="60"><img src="/public/img/bucket_icon.png" alt="Huge bucket"/></a>
    <br/>
    <a href="#SketchPad" data-download='png' id="DownloadPng">Download .PNG</a>
    <br/>
  </div>
  
  <canvas id="SketchPad" width="800" height="600"></canvas>

  <script type="text/javascript" src="/public/js/sketch.js">
    $(function() {
      $('#SketchPad').sketch();
    });
  </script>
  <script type="text/javascript" src="/public/js/sketch.js"></script>
  <script src="http://code.jquery.com/jquery-latest.js"></script>
  
        
</body>

</html>