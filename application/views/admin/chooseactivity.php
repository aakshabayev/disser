<div id="activity_container">
    <h1>
        Please choose an activity:
    </h1>
    <div>
        <table frame="box" class="ind_table_td">
            <tr>
                <td>
                    <p>
                        <a href="/activity/createmultiple"><input type="submit" name="submit" value="Multiple votes" class="btn"></a>(Activity for <font color="red">multiple questions and voting answers</font>)
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>
                        <a href="/activity/createcompetition"><input type="submit" name="submit" value="Competition" class="btn"></a> (Activity for <font color="red">online competition</font> with <font color="red">real-time board</font> and <font color="red">chat</font>)
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>
                        <a href="/activity/createdrawing"><input type="submit" name="submit" value="Online drawing" class="btn"></a>(Activity for real-time <font color="red">participatory drawing</font> for solving some issues, such as drawing Prototypes with online chat)
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>
                        <a href="/activity/create"><input type="submit" name="submit" value="Quick feedback" class="btn"></a>(Activity for quick voting <font color="red">(YES, NO)</font> type of question)
                    </p>
                </td>
             </tr>
        </table>
    </div>
</div>
    