      <div id='color-btns'>
        <font size="5"><u>Group number: #<font color="red"><?=$group->id?></u></font><br></font>
        Choose a color:
            <input type='button' style="background:black;" id='black-btn' value='     '>
            <input type='button' style="background:red;" id='red-btn' value='     '>
            <input type='button' style="background:green;" id='green-btn' value='     '>
            <input type='button' style="background:blue;" id='blue-btn' value='     '>
            <input type='button' style="background:yellow;" id='yellow-btn' value='     '>
            <input type='button' style="background:white;" id='white-btn' value='     '>
        <div>
        Choose width:
            <div>
            <input type="range" name="points" min="0" max="10" id='line-width'>
           </div>
           </div>
            <!-- <input type='text' id='line-width'> -->
        </div>
        <div id="can">
            <canvas id="paper" width="1200" height="300">
        </div>


        </canvas>
        <div>
            Online chat:
            <textarea style="width:100%; border-style: solid; border-color: #0000ff;" id='log' name='log' readonly='readonly'></textarea><br/>
            <input type='text' id='message' placeholder="Message..." name='message' />
            <div>
                (Computer: Press "Enter" to send message)<br>
                (Mobile: Press "Return" to send message)

            </div>
        </div>

<script src='//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

<script type="text/javascript" src="/public/js/my_websocket.js"></script>

<script type="text/javascript">
$(function(){

    // This demo depends on the canvas element
    if(!('getContext' in document.createElement('canvas'))){
        alert('Sorry, it looks like your browser does not support canvas!');
        return false;
    }

    // The URL of your web server (the port is set in app.js)

    var doc = $(document),
        win = $(window),
        canvas = $('#paper'),
        ctx = canvas[0].getContext('2d'),
        instructions = $('#instructions');

    // Generate an unique ID
    var id = Math.round($.now()*Math.random());

    // A flag for drawing activity
    var drawing = false;

    var clients = new Array(1000);
    var x_arr = new Array(1000);
    var y_arr = new Array(1000);

    // start websocket
    var activity_id = "<?=$activity->id?>";
    var group_id = "<?=$group->id?>";
    var user_id = "<?=$user = Auth::instance()->get_user()?>";
    user_id = parseInt(user_id);
    var color = "black";
    var line_width = 2;
    var username_init = "<?=$user = Auth::instance()->get_user()->username?>";

    $("#black-btn").click(function() {
        color = "black";
    });
    $("#red-btn").click(function() {
        color = "red";
    });
    $("#green-btn").click(function() {
        color = "green";
    });
    $("#blue-btn").click(function() {
        color = "blue";
    });
    $("#yellow-btn").click(function() {
        color = "yellow";
    });
    $("#white-btn").click(function() {
        color = "white";
    });


    var Server;

    function send( text ) {
      Server.send( 'message', text );
    }

    console.log('Connecting...');
    Server = new MyWebSocket('ws://46.101.26.127:9300');
    // Server = new MyWebSocket('ws://127.0.0.1:9300');

    //Let the user know we're connected
    Server.bind('open', function() {
        send("page:draw,activity_id:" + activity_id + ",group:" + group_id + ",message:start");
        console.log( "Connected." );
    });

    //OH NOES! Disconnection occurred.
    Server.bind('close', function( data ) {
        console.log( "Disconnected." );
    });

    //Log any messages sent from server
    Server.bind('message', function( payload ) {
        var coordinates = payload.split('-');
        if (coordinates[0] == 'chat') {
            username = coordinates[1];
            chat_msg = coordinates[2];
            log(username + ": " + chat_msg);
        } else {
            var x = coordinates[0];
            var y = coordinates[1];
            var user_id = coordinates[2];
            var is_remote_draw = coordinates[3];
            if (is_remote_draw == "true") {
                remote_color = coordinates[4];
                remote_line_width = parseInt(coordinates[5]);
                header_height = parseInt(coordinates[6])
                drawLine(x_arr[user_id], y_arr[user_id] - header_height, x, y - header_height, remote_color, remote_line_width);
            }

            x_arr[user_id] = x;
            y_arr[user_id] = y;
        }

    });

    Server.connect();
// end websocket

    var prev = {};

    canvas.on('mousedown',function(e){
        e.preventDefault();
        send("page:draw,activity_id:" + activity_id + ",group:" + group_id + ",message:" + e.pageX + "-" + e.pageY + "-" + user_id + "-" + drawing + "-" + color + "-" + line_width);
        drawing = true;
        prev.x = e.pageX;
        prev.y = e.pageY;

        // Hide the instructions
        instructions.fadeOut();
    });

    doc.bind('mouseup mouseleave',function(e){
        drawing = false;
    });

    var lastEmit = $.now();

    doc.on('mousemove',function(e){
        if($.now() - lastEmit > 30 && drawing == true){
            header_height = $('.nav').height() + $('#color-btns').height();

            send("page:draw,activity_id:" + activity_id + ",group:" + group_id + ",message:" + e.pageX + "-" + e.pageY + "-" + user_id + "-" + drawing + "-" + color + "-" + line_width + "-" + header_height);
            lastEmit = $.now();
        }

        // Draw a line for the current user's movement, as it is
        // not received in the socket.on('moving') event above

        if(drawing){
            header_height = $('.nav').height() + $('#color-btns').height();
            line_width = parseInt($("#line-width").val());
            drawLine(prev.x, prev.y - header_height, e.pageX, e.pageY - header_height, color, line_width);

            prev.x = e.pageX;
            prev.y = e.pageY;
        }
    });

    function drawLine(fromx, fromy, tox, toy, color, line_width){
        ctx.beginPath();
        ctx.moveTo(fromx, fromy);
        ctx.lineTo(tox, toy);
        ctx.strokeStyle = color;
        ctx.lineWidth=line_width;
        ctx.stroke();
        ctx.closePath();
    }
    // chat page
    function log( text ) {
      $log = $('#log');
      //Add text to log
      $log.append(($log.val()?"\n":'')+text);
      //Autoscroll
      $log[0].scrollTop = $log[0].scrollHeight - $log[0].clientHeight;
    }

    $('#message').keypress(function(e) {
    if ( e.keyCode == 13 && this.value ) {
      log( 'You: ' + this.value );
      send("page:draw,activity_id:" + activity_id + ",group:" + group_id + ",message:chat" + "-" + username_init + "-" + this.value);

      $(this).val('');
    }
    });



});
</script>