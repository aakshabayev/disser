
<!DOCTYPE html><html class=''>
<head><meta charset='UTF-8'><meta name="robots" content="noindex"><link rel="canonical" href="http://codepen.io/ashblue/pen/KyvmA" />


<style class="cp-pen-styles">.invalid input:required:invalid {
    background: #BE4C54;
}

.invalid input:required:valid {
    background: #17D654 ;
}

input {
  display: block;
  margin-bottom: 10px;
}</style></head><body>
<h1>Cross Browser HTML5 Validation</h1>
<p>Works in the latest version of all browsers. <strong>Note</strong> Codepen iframes may cause issues with validation messages showing.</p>
<form class="validate-form">
  <input type="text" 
         title="required input" 
         placeholder="required" 
         required />
  <input type="text" 
         title="Must be 5 numeric numbers" 
         placeholder="zip code" 
         pattern="\d{5}"
         maxlength="5"
         required />
  <input type="submit" />
</form>
<p>Status: <span id="status">Unsubmitted</span></p>

<script src='//assets.codepen.io/assets/common/stopExecutionOnTimeout.js?t=1'></script>
<script src='//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script>
function hasHtml5Validation() {
    return typeof document.createElement('input').checkValidity === 'function';
}
if (hasHtml5Validation()) {
    $('.validate-form').submit(function (e) {
        if (!this.checkValidity()) {
            e.preventDefault();
            $(this).addClass('invalid');
            $('#status').html('invalid');
        } else {
            $(this).removeClass('invalid');
            $('#status').html('submitted');
        }
    });
}
//@ sourceURL=pen.js
</script>
<script src='//codepen.io/assets/editor/live/css_live_reload_init.js'></script>
</body></html>