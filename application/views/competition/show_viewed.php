<div id="cont">

  <div id="question">
    <h1>
      Group number: #<font color="red"><?=$group->id?></font><br><br>
      <font color="red">You have already answered this question!</font><br><br><br>
      <font color="green">Please choose question you didn't answer yet:</font><br><br>
    </h1>
  </div>

  <div id="boarding">
    <h2>
    Question:
    <?php
    for ($i = 1; $i <= $question_count; $i++) {
    ?>
     <a href="/activity/showcompetition?id=<?=$question_competition->activity_competition_id?>&order=<?=$i?>"><?=$i?> | </a>
    <?php
    }
    ?>
    </h2>
  </div>
  <br>
  <div style="clear:both; border: 3px solid #003300; border-radius:12px;">
    <h2>
    Online boarding:<br><br>
    <?php
    $k = 0;
    $i = 0;
    $sort = array();
    ?>
    <?php
    foreach ($users_ans as $key => $value) {
      $k = $k + 1;
      // $sort[$i] = $value;
      // $i = $i + 1;
      // rsort($sort, SORT_NUMERIC);
      // print_r($sort);
    ?>

      <div id="participants">
        <div style="float:left;"><?=$key?> -><font color="white">_</font></div> <div style="float:left;" id='<?=$key?>'><?=$value?></div> <div style="float:left;"><font color="white">_</font>correct out of<font color="white">_</font><?=$question_count?></div><br>
        <?php
        $result = ($value/$question_count)*100;
        ?>
        <style>
          [id='<?=$key . "_"?>'] {
          width: <?=$result?>%;
          height: 32px;
          background: #000066;
          border-radius:20px;
        }
        </style>
        <?php
        if ($value>0){
        ?>
          <font color="yellow"><p id="<?=$key . '_'?>"><?=round($result,0)?>%</p></font>
        <?php
        }else{
        ?>
          <font color="black"><p id="<?=$key . '_'?>">0%</p></font>
        <?php
        }
        ?>
      </div>
    <?php
    }
    ?>

    </h2>
  </div>



</div>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript" src="/public/js/my_websocket.js"></script>
  <script>
    var activity_id = "<?=$question_competition->activity_competition_id?>";
    var group_id = "<?=$group->id?>";
    var type = "<?=$question_competition->type?>";
    var user_id = "<?=$user = Auth::instance()->get_user()->username?>";
    var question_id = "<?=$question_competition->id?>";
    var question_count = "<?=$question_count?>";
    var Server;

    function send( text ) {
      Server.send( 'message', text );
    }

    $(document).ready(function() {
      Server = new MyWebSocket('ws://46.101.26.127:9300');
      // Server = new MyWebSocket('ws://127.0.0.1:9300');
      //Let the user know we're connected
      Server.bind('open', function() {
        send("page:competition,activity_id:" + activity_id + ",group:" + group_id + ",message:start");
        console.log( "Connected." );
      });

      //OH NOES! Disconnection occurred.
      Server.bind('close', function( data ) {
        console.log( "Disconnected." );
      });

      //Log any messages sent from server
      Server.bind('message', function( payload ) {
        console.log(payload);
        res = payload.split(":");
        var username = res[0];
        var correct = res[1];
        var user_id = '#' + username;

        if ($(user_id).length == 0) {
          location.reload();
          // if (correct == '0')
          //   $('#participants').append("<div id='"+username+"'>"+username+"-> 0 of "+question_count+" </div><font color='black'>0%</p></font>");
          // if (correct == '1')
          //   $('#participants').append("<div id='"+username+"'>"+username+"-> 1 of "+question_count+" </div><font color='yellow'><p id='"+username+"_'>"+(1/question_count*100)+"%</p></font>");
        } else {
          if (correct == '1') {
            var cnt = parseInt($(user_id).html());
            console.log(cnt);
            cnt++;
            $(user_id).html(cnt);
             
            var result = (cnt/question_count)*100;
            $(user_id + '_').width(result + "%");
            $(user_id + '_').html(Math.round(result,0) + "%"); 
            location.reload();
          }
        }
      });

      Server.connect();
    });
  </script>