<script src='//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

<div id="cont" align="center">
    <div id="question">
        <h1>


        <!-- <font color="red"><b>Dear, <?php $user = Auth::instance()->get_user()->username; echo($user)?>!</font></b> -->
        Group number: #<font color="red"><?=$group->id?></font><br><br><br>
        <font color="red"><b><?=$question_competition->question?></b></font>

        </h1>

        <div>
        <h2>

            <?php
            if ($question_competition->type == 'multiple') {
            ?>

                #1 : <?=$question_competition->answer1?><br><br>
                #2 : <?=$question_competition->answer2?><br><br>
                #3 : <?=$question_competition->answer3?><br><br>
                #4 : <?=$question_competition->answer4?><br><br>
                #5 : <?=$question_competition->answer5?><br><br>

            <?php
            }
            ?>

            <?php
            if ($question_competition->type == 'multiple') {
            ?>
                <form method="post" action='/activity/answercompetition'>
                    <input type='hidden' name='answer' id='answer'>
                    <input type='hidden' name='question_id' value='<?=$question_competition->id?>'>
                    <input type='hidden' name='activity_id' value='<?=$activity_competition->id?>'>
                    <input type='hidden' name='order' value='<?=$question_competition->order?>'>
                    <input type='submit' id='button1' value="1" class="btn">
                    <input type='submit' id='button2' value="2" class="btn">
                    <input type='submit' id='button3' value="3" class="btn">
                    <input type='submit' id='button4' value="4" class="btn">
                    <input type='submit' id='button5' value="5" class="btn">
                </form>
            <?php
            }
            ?>

            <?php
            if ($question_competition->type == 'open') {
            ?>

                <form method="post" action='/activity/answercompetition'>
                    <input type='text' name='answer' id='answer' autocomplete="off" required>
                    <input type='hidden' name='question_id' value='<?=$question_competition->id?>'>
                    <input type='hidden' name='activity_id' value='<?=$activity_competition->id?>'>
                    <input type='hidden' name='order' value='<?=$question_competition->order?>'>
                    <input type='submit' id='button6' value="submit" class="btn">
                </form>
                <br><br><br>
            <?php
            }
            ?>

            <?php
            if ($question_competition->type == 'yesno') {
            ?>
                <form method="post" action='/activity/answercompetition'>
                    <input type='hidden' name='answer' id='answer'>
                    <input type='hidden' name='question_id' value='<?=$question_competition->id?>'>
                    <input type='hidden' name='activity_id' value='<?=$activity_competition->id?>'>
                    <input type='hidden' name='order' value='<?=$question_competition->order?>'>
                    <!-- <input type='submit' id='button0' value="0" class="round-button">
                    <input type='submit' id='button1' value="1" class="round-button"> -->
                    <button name="yesno" id="button1" type="submit" value="1" class="btn">YES</button>
                    <button name="yesno" id="button0" type="submit" value="0" class="btn">NO</button>
                </form>
                <br><br><br>
            <?php
            }
            ?>
        </h2>
        </div>
    </div>

    <div id="boarding">
      <h2>
      Question:
      <?php
      for ($i = 1; $i <= $question_count; $i++) {
      ?>
       <a href="/activity/showcompetition?id=<?=$question_competition->activity_competition_id?>&order=<?=$i?>"><?=$i?> | </a>
      <?php
      }
      ?>
      </h2>
    </div>
    <br>
    <div style="clear:both; border: 3px solid #003300; border-radius:12px;">
        <h2>
        List of participants:<br><br>
        <?php
        $k = 0;
        $i = 0;
        $sort = array();
        ?>
        <?php
        foreach ($users_ans as $key => $value) {

            $k = $k + 1;
            // $sort[$i] = $value;
            // $i = $i + 1;
            // rsort($sort, SORT_NUMERIC);
            // print_r($sort);
            ?>

           <div id="participants">
            <div style="float:left;"><?=$key?>-><font color="white">_</font></div> <div style="float:left;" id='<?=$key?>'><?=$value?></div> <div style="float:left;"><font color="white">_</font>correct out of<font color="white">_</font><?=$question_count?></div><br>
            <?php
            $result = ($value/$question_count)*100;
            ?>
            <style>
            [id='<?=$key . "_"?>'] {
            width: <?=$result?>%;
            height: 40px;
            background: #000066;
            }
            </style>
            <?php
            if ($value>0){
            ?>
                <font color="yellow"><p id="<?=$key . '_'?>"><?=round($result,0)?>%</p></font>
            <?php
            }else{
            ?>
                <font color="black"><p id="<?=$key . '_'?>">0%</p></font>
            <?php
            }
            ?>
           </div>
        <?php
        }
        ?>
        </h2>
    </div>


</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript" src="/public/js/my_websocket.js"></script>

  <script>
    var activity_id = "<?=$activity_competition->id?>";
    var group_id = "<?=$group->id?>";
    var type = "<?=$question_competition->type?>";
    var user_id = "<?=$user = Auth::instance()->get_user()->username?>";
    var question_id = "<?=$question_competition->id?>";
    var question_count = "<?=$question_count?>"
    var Server;

    function send( text ) {
      Server.send( 'message', text );
    }

    $(document).ready(function() {
      console.log('Connecting...');
      Server = new MyWebSocket('ws://46.101.26.127:9300');
      // Server = new MyWebSocket('ws://127.0.0.1:9300');

        $('#button0').click(function() {
            $('#answer').val("0");
            send("page:competitionanswer,activity_id:" + activity_id + ",group:" + group_id + ",message:0," + "user:" + user_id + ",type:" + type + ",question_id:" + question_id);
        });
        $('#button1').click(function() {
            $('#answer').val("1");
            send("page:competitionanswer,activity_id:" + activity_id + ",group:" + group_id + ",message:1," + "user:" + user_id + ",type:" + type + ",question_id:" + question_id);
        });
        $('#button2').click(function() {
            $('#answer').val("2");
            send("page:competitionanswer,activity_id:" + activity_id + ",group:" + group_id + ",message:2," + "user:" + user_id + ",type:" + type + ",question_id:" + question_id);
        });
        $('#button3').click(function() {
            $('#answer').val("3");
            send("page:competitionanswer,activity_id:" + activity_id + ",group:" + group_id + ",message:3," + "user:" + user_id + ",type:" + type + ",question_id:" + question_id);
        });
        $('#button4').click(function() {
            $('#answer').val("4");
            send("page:competitionanswer,activity_id:" + activity_id + ",group:" + group_id + ",message:4," + "user:" + user_id + ",type:" + type + ",question_id:" + question_id);
        });
        $('#button5').click(function() {
            $('#answer').val("5");
            send("page:competitionanswer,activity_id:" + activity_id + ",group:" + group_id + ",message:5," + "user:" + user_id + ",type:" + type + ",question_id:" + question_id);
        });
        $('#button6').click(function() {
            send("page:competitionanswer,activity_id:" + activity_id + ",group:" + group_id + ",message:" + $('#answer').val() + ",user:" + user_id + ",type:" + type + ",question_id:" + question_id);
        });

      //Let the user know we're connected
      Server.bind('open', function() {
        send("page:competition,activity_id:" + activity_id + ",group:" + group_id + ",message:start");
        console.log( "Connected." );
      });

      //OH NOES! Disconnection occurred.
      Server.bind('close', function( data ) {
        console.log( "Disconnected." );
      });

      //Log any messages sent from server
      Server.bind('message', function( payload ) {
        console.log( payload );
        res = payload.split(":");
        var username = res[0];
        var correct = res[1];
        var user_id = '#' + username;

        if ($(user_id).length == 0) {
            location.reload();
          // if (correct == '0')
          //   $('#participants').append("<div id='"+username+"'>"+username+"-> 0 of "+question_count+" </div><div><font color='black'>0%</p></font></div>");
          // if (correct == '1')
          //   $('#participants').append("<div id='"+username+"'>"+username+"-> 1 of "+question_count+" </div><div><font color='yellow'><p id='"+username+"_'>"+(1/question_count*100)+"%</p></font></div>");
        } else {
            if (correct == '1') {
              var cnt = parseInt($(user_id).html());
              console.log(cnt);
              cnt++;
              $(user_id).html(cnt);

              var result = (cnt/question_count)*100;
              $(user_id + '_').width(result + "%");
              $(user_id + '_').html(Math.round(result,0) + "%");
              location.reload();  
          }
        }
      });

      Server.connect();
    });
  </script>
