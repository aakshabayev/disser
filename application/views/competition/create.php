<div id="container">
   	<h1>
       	<b>Create competition activity</b><br>
    </h1>
  	<form method="post" autocomplete="off"  class="validate-form">
      <h2>
        <font style="padding-right:40px">Activity name:</font><input name="activity_name" type="text"  placeholder="activity name" title="" style="background:yellow; width: 50%;" required>
      </h2>
      <div>
        <table frame="box" class="ind_table_td1" id="question_type_table">
          <tr>
            <td>
              <p align="center"><font color="red"><b>Choose type of question</b></font></p>
            </td>
            <td>
              <div style="float:left;">
                <select class="select_style" id="mySelect">
                  <option id="1" value="1">multiple answer</option>
                  <option id="2" value="2">open answer</option>
                  <option id="3" value="3">yes/no</option>
                </select>
              </div> 
              <div style="float:left;">
                <div style="float: right;">
                  <button type="button" id="add_more_question" onclick="addType()" class="okbtn">Add</button>
                  <button type="button" id="del_more_question" onclick="delType()" class="okbtn" style="background: #d3d3d3;" disabled>del</button>
                </div>
              </div>
            </td>
          </tr>
        </table>
        <input type="submit" id="complete" name="submit_activity" value="Complete" class="btn" style="background: #d3d3d3;" disabled>
      </div>
    </form>
</div>




<script type="text/javascript" src="/public/js/createcompetition.js"></script>



