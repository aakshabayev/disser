<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 5 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<title><?php echo $title; ?></title>
		<meta name="description" content="<?php echo $description; ?>" />
	    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
	    <meta name="description" content="Home page"/>
	    <meta name="author" content="Yernar Akshabayev"/>
        <!-- <meta name="robots" content="noindex"><link rel="canonical" href="http://codepen.io/ashblue/pen/KyvmA" /> -->
	    <title>Home</title>
	    <link href="/public/css/main.css" rel="stylesheet"/>
	    <link href="/public/css/tabs.css" rel="stylesheet"/>
	    <link href="/public/css/reg.css" rel="stylesheet"/>
	    <link href="/public/css/modal_box.css" rel="stylesheet"/>
        <link href="/public/css/competition.css" rel="stylesheet"/>
        <link href="/public/css/validation.css" rel="stylesheet"/>
</head>
<header>
    <div class="nav">
        <ul>

            <?php
            $user = Auth::instance()->get_user();
            if ($user !== null)
            {?>
                
                <li class="student"><a <?php if ($page == 'admin/welcomeadmin') echo 'class="active"' ?> href="/user/welcomeadmin"><?php $user1 = Auth::instance()->get_user()->username; echo($user1)?></a></li>
                <li class="choose"><a <?php if ($page == 'admin/chooseactivity') echo 'class="active"' ?> href="<?php echo URL::base() ?>user/chooseactivity">Create</a></li>
                <li class="participate"><a <?php if ($page == 'groupjoin') echo 'class="active"' ?> href="<?php echo URL::base() ?>user/participate">Participate</a></li>
                <li class="list"><a <?php if ($page == 'activity/list') echo 'class="active"' ?> href="<?php echo URL::base() ?>activity/list">View</a></li>

                <?php
            }else{?>
                <li class="home"><a <?php if ($page == 'welcome') echo 'class="active"' ?> href="<?php echo URL::base() ?>welcome">Home</a></li>
                
                <li class="teacher"><a <?php if ($page == 'teacher') echo 'class="active"' ?> href="<?php echo URL::base() ?>teacher">Creator</a></li>
                <li class="teacher"><a <?php if ($page == 'student') echo 'class="active"' ?> href="<?php echo URL::base() ?>student">Participant</a></li>
                <li class="teacher"><a <?php if ($page == 'register') echo 'class="active"' ?> href="<?php echo URL::base() ?>register">Register</a></li>
                <li class="about"><a <?php if ($page == 'about') echo 'class="active"' ?> href="<?php echo URL::base() ?>about">Help</a></li>
                <li class="about"><a <?php if ($page == 'about') echo 'class="active"' ?> href="<?php echo URL::base() ?>about">Contacts</a></li>
            <?php
            }?>

            <?php

            $user = Auth::instance()->get_user();
            if ($user !== null)
            {?>
                <li class="student"><a <?php if ($page == 'student') echo 'class="active"' ?> href="/user/logout">Log out</a></li>
                <li class="about"><a <?php if ($page == 'about') echo 'class="active"' ?> href="<?php echo URL::base() ?>about">Help</a></li>
            <?php
            }?>
        </ul>
    </div>
</header>

<body>


    <?php echo $content; ?>
    </div>

    <div >
        <div class="info" style="padding-top: ">
            Hope this site will be useful for you :))
        </div>
        <div class="foo" align="center">
            © 2015 All Rights Reserved by Yernar Akshabayev
        </div>
    </div>

    <script src='//assets.codepen.io/assets/common/stopExecutionOnTimeout.js?t=1'></script>
    <script src='//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script type="text/javascript" src="/public/js/validation.js"></script>
    <script type="text/javascript" src="/public/js/my_websocket.js"></script>
    <script type="text/javascript" src="/public/js/createmultiple.js"></script>

    <script src='//codepen.io/assets/editor/live/css_live_reload_init.js'></script>


</body>
</html>