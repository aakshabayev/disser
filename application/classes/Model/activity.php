<?php defined('SYSPATH') or die('No direct script access.');

class Model_Activity extends ORM {
	protected $_table_name = 'activity';
    protected $_primary_key = 'activity_id';
}