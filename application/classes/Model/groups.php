<?php defined('SYSPATH') or die('No direct script access.');

class Model_Groups extends ORM {
    protected $_table_name = 'groups';
    protected $_primary_key = 'id';
    protected $_belongs_to = array(
        'activity'    => array(
           'model'=>'activity',
           'foreign_key' =>   'activity_id',
        )
    );


}