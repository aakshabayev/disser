<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Register extends Controller_Template {

	public function action_index()
	{
		$content = View::factory('register');
		$this->template->title = "Registration";
		$this->template->description = "description of the site";
		$this->template->page = 'register';
		$this->template->content = $content;
	}

	
} // End Welcome
