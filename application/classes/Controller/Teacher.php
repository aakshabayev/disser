<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Teacher extends Controller_Template {

	public function action_index()
	{
		if(Auth::instance()->logged_in()) {
            $this->redirect('/user/welcomeadmin');
        }
		$content = View::factory('teacher');
		$this->template->title = "login";
		$this->template->description = "description of the site";
		$this->template->page = 'teacher';
		$this->template->content = $content;
	}
} // End Welcome
