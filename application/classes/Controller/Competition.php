<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Competition extends Controller_Template {

	public function action_index()
	{
		$content = View::factory('/competition/create');
		$this->template->title = "hi, this is title of the site";
		$this->template->description = "description of the site";
		$this->template->page = 'teacher';
		$this->template->content = $content;
	}

	public function action_show()
	{
		$content = View::factory('/competition/show');
		$this->template->title = "hi, this is title of the site";
		$this->template->description = "description of the site";
		$this->template->page = 'teacher';
		$this->template->content = $content;
	}
} // End Welcome
