<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Welcome extends Controller_Template {

	public function action_index()
	{
		if(Auth::instance()->logged_in()) {
            $this->redirect('welcome/logged');
        }
		$content = View::factory('welcome');
		$this->template->title = "Online participation web site";
		$this->template->description = "description of the site";
		$this->template->page = 'welcome';
		$this->template->content = $content;
	}

	public function action_logged()
	{
		$content = View::factory('admin/welcomeadmin');
		$this->template->title = "Online participation web site";
		$this->template->description = "description of the site";
		$this->template->page = 'welcome';
		$this->template->content = $content;
	}


} // End Welcome
