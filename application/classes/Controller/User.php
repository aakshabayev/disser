<?php defined('SYSPATH') or die('No direct script access.');

class Controller_User extends Controller_Template {

	public function action_index()
	{
		$content = View::factory('student');
		$this->template->title = "hi, this is title of the site";
		$this->template->description = "description of the site";
		$this->template->page = 'student';
		$this->template->content = 'hello world';
	}
	public function action_register()
	{
		$this->template->title = "register";
		$this->template->description = "description of the site";
		$this->template->page = 'teacher';
		try{
			if (isset($_POST)){
				$user = Arr::get($this->request->post(), "username");
				$email = Arr::get($this->request->post(), "email");
				$password = Arr::get($this->request->post(), "password");
				$newuser = ORM::factory("User");
				$newuser->username = $user;
				$newuser->password = $password;
				$newuser->email = $email;
				$newuser->save();
				$role = Arr::get($this->request->post(), "role");

				$newuser->add('roles', ORM::factory('Role')->where('name', '=', $role)->find());
			}
			$content = View::factory('user/reg_success');
			$this->template->content = $content;
		}
		catch(ORM_Validation_Exception $e)
		{
			$errors = $e->errors();
			$content = View::factory('user/reg_error');
			$this->template->content = $content;
		}
	}

	public function action_error()
	{
		$content = View::factory('user/group_error');
		$this->template->title = "error";
		$this->template->description = "description of the site";
		$this->template->page = 'user/group_error';
		$this->template->content = $content;
	}

	public function action_participate()
	{
		if(!Auth::instance()->logged_in()) {
            $this->redirect('welcome');
        }
		$content = View::factory('groupjoin');
		$this->template->title = "error";
		$this->template->description = "description of the site";
		$this->template->page = 'user/group_error';
		$this->template->content = $content;
	}

	public function action_login(){
		$this->template->title = "login";
		$this->template->description = "description of the site";
		$this->template->page = 'teacher';

      	$post = $this->request->post();
		$success = Auth::instance()->login($post['username'], $post['password']);

		if ($success || Auth::instance()->logged_in()){
			if (isset($post['group_id'])) {
				echo "hie2";
				$group = 0;
				$group_id = $post['group_id'];
				$group = ORM::factory("groups")->where('id', '=', $group_id)->find();
				$groupcount = 0;
				$groupcount = ORM::factory("groups")->where('id', '=', $group_id)->count_all();
				if ($groupcount==0) {
					Auth::instance()->logout(FALSE, TRUE);
					$this->redirect('user/error');
				}

				$activity_id = $group->activity_id;
				if ($group->type == '1') {
					$this->redirect('activity/show?id='.$activity_id);
				}
				if ($group->type == '2') {
					$this->redirect('activity/showmultiple?id='.$activity_id . "&order=1");
				}
				if ($group->type == '3') {
					$this->redirect('activity/showcompetition?id='.$activity_id . "&order=1");
				}
				if ($group->type == '4') {
					$this->redirect('activity/drawing?id='.$activity_id);
				}
			}
			$content = View::factory('user/login_success');
			$this->template->content = $content;
		}else{
			$content = View::factory('user/login_error');
			$this->template->content = $content;
		}

	}

	public function action_logout(){
		Auth::instance()->logout(FALSE, TRUE);
		$this->redirect('welcome');
	}

	public function action_welcomeadmin(){
		if(!Auth::instance()->logged_in()) {
            $this->redirect('welcome');
        }
		$content = View::factory('admin/welcomeadmin');
		$this->template->title = "welcome";
		$this->template->description = "description of the site";
		$this->template->page = 'admin/welcomeadmin';
		$this->template->content = $content;

	}

	public function action_chooseactivity(){
		if(!Auth::instance()->logged_in()) {
            $this->redirect('welcome');
        }
		$content = View::factory('admin/chooseactivity');
		$this->template->title = "choose an activity";
		$this->template->description = "description of the site";
		$this->template->page = 'admin/chooseactivity';
		$this->template->content = $content;

	}


	// public function action_login()
	// {
	// 	$content = View::factory('user/register');
	// 	$this->template->title = "hi, this is title of the site";
	// 	$this->template->description = "description of the site";
	// 	if ($this->request->method() !== HTTP_Request::POST) {
 //   			$this->redirect('/teacher');
 //   			return;
 //  		}

 //  		if (Auth::instance()->logged_in()) {
 //   			$this->redirect('/user/register');
 //  		}

 //  		$post = $_POST;
 //  		//$user = ORM::factory('user')->where('email', '=', $post['email'])->find();

	// 	$success = Auth::instance()->login($post['email'], $post['password']);

	// 	print_r($success);

	// 	if ($success){
	// 		//$this->redirect('/user/register');
	// 		echo('ok');
	// 	}else{
	// 		//$this->redirect('/teacher');
	// 		echo('not ok');
	// 	}

	// 	$this->template->content = $content;
	// }

} // End Welcome
