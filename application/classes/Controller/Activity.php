<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Activity extends Controller_Template {

	public function action_index()
	{
		$content = View::factory('activity/createactivity');
		$this->template->title = "create activity";
		$this->template->description = "description of the site";
		$this->template->page = 'createactivity';
		$this->template->content = $content;
	}

	public function action_create(){
		if ($this->request->method() == HTTP_Request::POST){
			$activity = ORM::factory('activity');
			$activity->name = Arr::get($this->request->post(), "name");
			$activity->question = Arr::get($this->request->post(), "question");
			$activity->deadline = Arr::get($this->request->post(), "deadline");
			$activity->size = Arr::get($this->request->post(), "size");
			$user_id = Auth::instance()->get_user();
			$activity->user_id = $user_id;
			$activity->save();

			$group = ORM::factory('groups');
			$group->activity_id = $activity->activity_id;
			$group->type = 1;
			$group->save();

			$this->redirect('activity/show?id='.$activity->activity_id);

		}

		$content = View::factory('activity/createactivity');
		$this->template->title = "create activity";
		$this->template->description = "description of the site";
		$this->template->page = 'createactivity';
		$this->template->content = $content;

	}

	public function action_show(){
		$this->template->title = "show activity";
		$this->template->description = "description of the site";
		$this->template->page = 'createactivity';

		if(!Auth::instance()->logged_in()) {
            $this->redirect('welcome');
        }
        $user_id = Auth::instance()->get_user();
        $activity_id = $this->request->query('id');

		$usersanswer_count = ORM::factory('usersanswer')->where('user_id', '=', $user_id)->where('activity_id', '=', $activity_id)->count_all();
/////////////////////////////////
		$group = ORM::factory('groups')
			->where('activity_id','=', $activity_id)
			->where('type', '=', '1')
			->find();
/////////////////////////////////
		$id = $this->request->query('id');
		if($usersanswer_count>0){
			$activity = ORM::factory('activity')->where('activity_id', '=', $id)->find();
			//$this->template->content = View::factory('activity/alreadyviewed')->set('activity', $activity);
			$this->redirect('activity/result?id='.$id);
		}else{
			$activity = ORM::factory('activity')->where('activity_id', '=', $id)->find();
			$this->template->content = View::factory('activity/show')
			->set('activity', $activity)
			->set('group', $group);
		}
	}

	public function action_yesno(){
		if ($this->request->method() == HTTP_Request::POST){
			$user_id = Auth::instance()->get_user();
			$activity_id = Arr::get($this->request->post(), "activity_id");
			// check user already voted or not
			$usersanswer_count = ORM::factory('usersanswer')->where('user_id', '=', $user_id)->where('activity_id', '=', $activity_id)->count_all();

			if ($usersanswer_count == 0) {
				$usersanswer = ORM::factory('usersanswer');
				$usersanswer->activity_id = $activity_id;
				$usersanswer->answer = Arr::get($this->request->post(), "yesno");
				$usersanswer->user_id = $user_id;
				$usersanswer->save();
			}
			$this->redirect('activity/result?id='.$usersanswer->activity_id);
		}

		$content = View::factory('yesno');
		$this->template->title = "yes no";
		$this->template->description = "description of the site";
		$this->template->page = 'createactivity';
		$this->template->content = $content;

	}


	public function action_list(){

		$this->template->title = "list of created activities";
		$this->template->description = "description of the site";
		$this->template->page = 'createactivity';

		$user_id = Auth::instance()->get_user();
		$activities = ORM::factory('activity')
		->select('activity.activity_id', 'activity.name', 'activity.date', array('groups.id', 'gid'))
		->join('groups')
		->on('activity.activity_id', '=', DB::Expr('groups.activity_id AND groups.type = 1'))
		->where('user_id', '=', $user_id)
		->find_all();

		$activities_multiple = ORM::factory('activitymultiple')
		->select('activitymultiple.id', 'activitymultiple.name', 'activitymultiple.date', array('groups.id', 'gid'))
		->join('groups')
		->on('activitymultiple.id', '=', DB::Expr('groups.activity_id AND groups.type = 2'))
		->where('user_id', '=', $user_id)
		->find_all();

		$activity_competition = ORM::factory('activitycompetition')
		->select('activitycompetition.id', 'activitycompetition.name', 'activitycompetition.date', array('groups.id', 'gid'))
		->join('groups')
		->on('activitycompetition.id', '=', DB::Expr('groups.activity_id AND groups.type = 3'))
		->where('user_id', '=', $user_id)
		->find_all();

		$activity_drawing = ORM::factory('drawing')
		->select('drawing.id', 'drawing.name', 'drawing.date', array('groups.id', 'gid'))
		->join('groups')
		->on('drawing.id', '=', DB::Expr('groups.activity_id AND groups.type = 4'))
		->where('user_id', '=', $user_id)
		->find_all();


		$this->template->activities = $activities;
		$this->template->content = View::factory('activity/list')
     	->set('activities', $activities)
     	->set('activities_multiple', $activities_multiple)
     	->set('activity_competition', $activity_competition)
     	->set('activity_drawing', $activity_drawing);
	}


	public function action_result(){
		$this->template->title = "result";
		$this->template->description = "description of the site";
		$this->template->page = 'createactivity';

		if(!Auth::instance()->logged_in()) {
            $this->redirect('welcome');
        }
        $user_id = Auth::instance()->get_user();
        $activity_id = $this->request->query('id');

        //$activity_question = $this->request->query('question');



        $activity = ORM::factory('activity')->where('activity_id', '=', $activity_id)->find();

        // if($activity->user_id != $user_id){
        // 	$this->redirect('welcome');
        // }

        $total_yes = ORM::factory('usersanswer')->where('answer','=','1')->and_where('activity_id','=',$activity_id)->find_all()->count();
		$total_no = ORM::factory('usersanswer')->where('answer','=','0')->and_where('activity_id','=',$activity_id)->find_all()->count();


		//$activity_question = ORM::factory('activity')->where('question', '=', $activity_question)->find();
		/////////////////////////////////
		$group = ORM::factory('groups')
			->where('activity_id','=', $activity_id)
			->where('type', '=', '1')
			->find();
/////////////////////////////////
		$this->template->content = View::factory('activity/result')
     	->set('total_yes', $total_yes)
		->set('total_no', $total_no)
		->set('activity', $activity)
		->set('group', $group);
	}

	public function action_createmultiple(){
		if ($this->request->method() == HTTP_Request::POST){
			if(!Auth::instance()->logged_in()) {
	            $this->redirect('welcome');
	        }
	        $user_id = Auth::instance()->get_user();

			$activity_multiple = ORM::factory('activitymultiple');
			$activity_multiple->name = Arr::get($this->request->post(), "activity_name");
			$activity_multiple->user_id = $user_id;
			$activity_multiple->save();

			$activity_id = $activity_multiple->id;

			$group = ORM::factory('groups');
			$group->activity_id = $activity_id;
			$group->type = 2;
			$group->save();

			$i = 1;
			while (isset($_POST['question_'. $i])) {
				$question_multiple = ORM::factory('questionmultiple');
				$question_multiple->activity_multiple_id = $activity_id;
				$question_multiple->question = $_POST['question_'. $i];
				$question_multiple->answer1 = $_POST['answer_'. $i . "_1"];
				$question_multiple->answer2 = $_POST['answer_'. $i . "_2"];
				$question_multiple->answer3 = $_POST['answer_'. $i . "_3"];
				$question_multiple->order = $i;
				$question_multiple->save();
				$i++;
			}
			$this->redirect('activity/showmultiple?id='.$activity_id.'&order=1');
		}

		$content = View::factory('activitymultiple/createmultiple');
		$this->template->title = "create multiple votes";
		$this->template->description = "description of the site";
		$this->template->page = 'createmultiple';
		$this->template->content = $content;
	}

	public function action_showmultiple(){
		$this->template->title = "show multiple votes";
		$this->template->description = "description of the site";
		$this->template->page = 'showmultiple';

		if(!Auth::instance()->logged_in()) {
            $this->redirect('welcome');
        }
        $user_id = Auth::instance()->get_user();
        $activity_id = $this->request->query('id');
        $order = $this->request->query('order');


        $question_multiple = ORM::factory('questionmultiple')
        					->where('activity_multiple_id', '=', $activity_id)
        					->and_where('order','=', $order)
        					->find();

		$question_id = $question_multiple->id;

		$usersanswer_count = ORM::factory('usersanswermultiple')
							->where('user_id', '=', $user_id)
							->where('question_multiple_id', '=', $question_id)
							->count_all();

		$question_count = ORM::factory('questionmultiple')
							->where('activity_multiple_id', '=', $activity_id)
							->count_all();
		$group = ORM::factory('groups')
							->where('activity_id','=', $activity_id)
							->where('type', '=', '2')
							->find();

		if($usersanswer_count>0){
			$answer1 = ORM::factory('usersanswermultiple')
			->where('question_multiple_id', '=', $question_id)
			->and_where('answer', '=', '1')
			->count_all();

			$answer2 = ORM::factory('usersanswermultiple')
			->where('question_multiple_id', '=', $question_id)
			->and_where('answer', '=', '2')
			->count_all();

			$answer3 = ORM::factory('usersanswermultiple')
			->where('question_multiple_id', '=', $question_id)
			->and_where('answer', '=', '3')
			->count_all();

			$this->template->content = View::factory('activitymultiple/show_viewed')
			->set('question_multiple', $question_multiple)
			->set('answer1', $answer1)
			->set('answer2', $answer2)
			->set('answer3', $answer3)
			->set('question_count', $question_count)
			->set('group', $group);

		}else{
			$activity_multiple = ORM::factory("activitymultiple")
							->where('id', '=', $activity_id)
							->find();
			$this->template->content = View::factory('activitymultiple/show')
			->set('question_multiple', $question_multiple)
			->set('activity_multiple', $activity_multiple)
			->set('question_count', $question_count)
			->set('group', $group);
		}
	}

	public function action_resultmultiple(){
		$this->template->title = "show multiple votes";
		$this->template->description = "description of the site";
		$this->template->page = 'showmultiple';

		if(!Auth::instance()->logged_in()) {
            $this->redirect('welcome');
        }
        $user_id = Auth::instance()->get_user();
        $activity_id = $this->request->query('id');
        $order = $this->request->query('order');


        $question_multiple = ORM::factory('questionmultiple')
        					->where('activity_multiple_id', '=', $activity_id)
        					->and_where('order','=', $order)
        					->find();

		$question_id = $question_multiple->id;

		$usersanswer_count = ORM::factory('usersanswermultiple')
							->where('user_id', '=', $user_id)
							->where('question_multiple_id', '=', $question_id)
							->count_all();

		$question_count = ORM::factory('questionmultiple')
							->where('activity_multiple_id', '=', $activity_id)
							->count_all();

		$group = ORM::factory('groups')
							->where('activity_id','=', $activity_id)
							->where('type', '=', '2')
							->find();

		$answer1 = ORM::factory('usersanswermultiple')
		->where('question_multiple_id', '=', $question_id)
		->and_where('answer', '=', '1')
		->count_all();

		$answer2 = ORM::factory('usersanswermultiple')
		->where('question_multiple_id', '=', $question_id)
		->and_where('answer', '=', '2')
		->count_all();

		$answer3 = ORM::factory('usersanswermultiple')
		->where('question_multiple_id', '=', $question_id)
		->and_where('answer', '=', '3')
		->count_all();

		$this->template->content = View::factory('activitymultiple/show_viewed')
		->set('question_multiple', $question_multiple)
		->set('answer1', $answer1)
		->set('answer2', $answer2)
		->set('answer3', $answer3)
		->set('question_count', $question_count)
		->set('group', $group);
	}

	public function action_answermultiple() {
		if ($this->request->method() == HTTP_Request::POST){
			$user_id = Auth::instance()->get_user();
			$question_id = Arr::get($this->request->post(), "question_id");
			// check user already voted or not
			$usersanswer_count = ORM::factory('usersanswermultiple')
								->where('user_id', '=', $user_id)
								->where('question_multiple_id', '=', $question_id)
								->count_all();

			if ($usersanswer_count == 0) {
				$usersanswer = ORM::factory('usersanswermultiple');
				$usersanswer->question_multiple_id = $question_id;
				$usersanswer->answer = Arr::get($this->request->post(), "answer");
				$usersanswer->user_id = $user_id;
				$usersanswer->save();
			}
			$activity_id = Arr::get($this->request->post(), "activity_id");
			$order = Arr::get($this->request->post(), "order");
			$this->redirect('activity/showmultiple?id='.$activity_id . "&order=". $order);
		}

		$content = View::factory('yesno');
		$this->template->title = "users answer multiple";
		$this->template->description = "description of the site";
		$this->template->page = 'createactivity';
		$this->template->content = $content;


	}

	public function action_createcompetition()
	{
		if ($this->request->method() == HTTP_Request::POST){
			if(!Auth::instance()->logged_in()) {
	            $this->redirect('welcome');
	        }
	        $user_id = Auth::instance()->get_user();

			$activity_competition = ORM::factory('activitycompetition');
			$activity_competition->name = Arr::get($this->request->post(), "activity_name");
			$activity_competition->user_id = $user_id;
			$activity_competition->save();

			$activity_id = $activity_competition->id;

			$group = ORM::factory('groups');
			$group->activity_id = $activity_id;
			$group->type = 3;
			$group->save();

			$i = 1;
			while (isset($_POST['question_'. $i])) {
				$question_competition = ORM::factory('questioncompetition');
				$question_competition->activity_competition_id = $activity_id;
				$question_competition->question = $_POST['question_'. $i];
				$question_competition->type = $_POST['type_' . $i];

				if ($_POST['type_' . $i] == 'multiple') {
					$question_competition->answer1 = $_POST['answer_'. $i . "_1"];
					$question_competition->answer2 = $_POST['answer_'. $i . "_2"];
					$question_competition->answer3 = $_POST['answer_'. $i . "_3"];
					$question_competition->answer4 = $_POST['answer_'. $i . "_4"];
					$question_competition->answer5 = $_POST['answer_'. $i . "_5"];
					$question_competition->answer_correct = $_POST['answer_'. $i];
				}

				if ($_POST['type_' . $i] == 'open') {
					$question_competition->answer1 = $_POST['answer_'. $i . "_1"];
					$question_competition->answer2 = $_POST['answer_'. $i . "_2"];
					$question_competition->answer3 = $_POST['answer_'. $i . "_3"];
					$question_competition->answer4 = $_POST['answer_'. $i . "_4"];
					$question_competition->answer5 = $_POST['answer_'. $i . "_5"];
				}

				if ($_POST['type_' . $i] == 'yesno') {
					$question_competition->answer_correct = $_POST['answer_'. $i];
				}


				$question_competition->order = $i;
				$question_competition->save();
				$i++;
			}
			$this->redirect('activity/showcompetition?id='.$activity_id.'&order=1');
		}

		$content = View::factory('competition/create');
		$this->template->title = "create activity";
		$this->template->description = "description of the site";
		$this->template->page = 'createactivity';
		$this->template->content = $content;
	}

	public function action_showcompetition()
	{
		$this->template->title = "show competition votes";
		$this->template->description = "description of the site";
		$this->template->page = 'showcompetition';

		if(!Auth::instance()->logged_in()) {
            $this->redirect('welcome');
        }
        $user_id = Auth::instance()->get_user();
        $activity_id = $this->request->query('id');
        $order = $this->request->query('order');


        $question_competition = ORM::factory('questioncompetition')
        					->where('activity_competition_id', '=', $activity_id)
        					->and_where('order','=', $order)
        					->find();

		$question_id = $question_competition->id;

		$usersanswer_count = ORM::factory('usersanswercompetition')
							->where('user_id', '=', $user_id)
							->where('question_competition_id', '=', $question_id)
							->count_all();

		$question_count = ORM::factory('questioncompetition')
							->where('activity_competition_id', '=', $activity_id)
							->count_all();
		$group = ORM::factory('groups')
							->where('activity_id','=', $activity_id)
							->where('type', '=', '3')
							->find();

		$answers_all = ORM::factory('usersanswercompetition')
							->select('usersanswercompetition.user_id', 'usersanswercompetition.question_competition_id', 'users.username')
							->join('users')
							->on('usersanswercompetition.user_id', '=', 'users.id')
							->order_by('user_id')
							->find_all();

		$users_ans = array();
		foreach ($answers_all as $key => $user_answer) {
			$question = ORM::factory('questioncompetition')
			->where('id', '=', $user_answer->question_competition_id)
			->find();

			if ($question->activity_competition_id == $activity_id) {

				$correct = 0;
				if ($question->type == 'multiple' && (string)($question->answer_correct) == $user_answer->answer) {
					$correct = 1;
				}

				if ($question->type == 'yesno' && (string)($question->answer_correct) == $user_answer->answer) {
					$correct = 1;
				}

				if ($question->type == 'open') {
					$str = $user_answer->answer;
					$str=preg_replace('/\s+/', '', $str);
					$str = strtolower($str);

					$str1 = $question->answer1;
					$str1=preg_replace('/\s+/', '', $str1);
					$str1 = strtolower($str1);

					$str2 = $question->answer2;
					$str2=preg_replace('/\s+/', '', $str2);
					$str2 = strtolower($str2);

					$str3 = $question->answer3;
					$str3=preg_replace('/\s+/', '', $str3);
					$str3 = strtolower($str3);

					$str4 = $question->answer4;
					$str4=preg_replace('/\s+/', '', $str4);
					$str4 = strtolower($str4);

					$str5 = $question->answer5;
					$str5=preg_replace('/\s+/', '', $str5);
					$str5 = strtolower($str5);

					if ($str == $str1 || $str == $str2 || $str == $str3 || $str == $str4 || $str == $str5)
						$correct = 1;
				}

				if ($key == 0) {
					$users_ans[$user_answer->username] = $correct;
				}
				else {
					if ($answers_all[$key]->user_id == $prev_user_id)
						$users_ans[$user_answer->username] = $users_ans[$user_answer->username] + $correct;
					else
						$users_ans[$user_answer->username] = $correct;
				}
				$prev_user_id = $answers_all[$key]->user_id;
			}
		}


		if($usersanswer_count>0){
			$this->template->content = View::factory('competition/show_viewed')
			->set('question_count', $question_count)
			->set('question_competition', $question_competition)
			->set('users_ans', $users_ans)
			->set('group', $group);

		}else{
			$activity_competition = ORM::factory("activitycompetition")
							->where('id', '=', $activity_id)
							->find();
			$this->template->content = View::factory('competition/show')
			->set('question_competition', $question_competition)
			->set('activity_competition', $activity_competition)
			->set('question_count', $question_count)
			->set('users_ans', $users_ans)
			->set('group', $group);
		}
	}

	public function action_showdrawing()
	{
		$content = View::factory('activitydrawing/show');
		$this->template->title = "create activity";
		$this->template->description = "description of the site";
		$this->template->page = 'createactivity';
		$this->template->content = $content;
	}


	public function action_answercompetition() {
		if ($this->request->method() == HTTP_Request::POST){
			$user_id = Auth::instance()->get_user();
			$question_id = Arr::get($this->request->post(), "question_id");
			// check user already voted or not
			$usersanswer_count = ORM::factory('usersanswercompetition')
								->where('user_id', '=', $user_id)
								->where('question_competition_id', '=', $question_id)
								->count_all();

			if ($usersanswer_count == 0) {
				$usersanswer = ORM::factory('usersanswercompetition');
				$usersanswer->question_competition_id = $question_id;
				$usersanswer->answer = Arr::get($this->request->post(), "answer");
				$usersanswer->user_id = $user_id;
				$usersanswer->save();
			}
			$activity_id = Arr::get($this->request->post(), "activity_id");
			$order = Arr::get($this->request->post(), "order");
			$this->redirect('activity/showcompetition?id='.$activity_id . "&order=". $order);
		}
	}
	public function action_createdrawing()
	{
		if ($this->request->method() == HTTP_Request::POST){
			$activity_drawing = ORM::factory('drawing');
			$activity_drawing->name = Arr::get($this->request->post(), "name");
			$user_id = Auth::instance()->get_user();
			$activity_drawing->user_id = $user_id;
			$activity_drawing->save();

			$activity_id = $activity_drawing->id;

			$group = ORM::factory('groups');
			$group->activity_id = $activity_id;
			$group->type = 4;
			$group->save();

			$this->redirect('activity/drawing?id='.$activity_drawing->id);

		}

		$content = View::factory('activitydrawing/create');
		$this->template->title = "create activity";
		$this->template->description = "description of the site";
		$this->template->page = 'createactivity';
		$this->template->content = $content;
	}

	public function action_drawing()
	{
		// if(!Auth::instance()->logged_in()) {
  //           $this->redirect('welcome');
  //       }
		// $content = View::factory('draw');
		// $this->template->title = "draw activity";
		// $this->template->description = "draw activity";
		// $this->template->page = 'drawactivity';
		// $this->template->content = $content;

		//////////////////////////

		$this->template->title = "show activity";
		$this->template->description = "description of the site";
		$this->template->page = 'createactivity';

		if(!Auth::instance()->logged_in()) {
            $this->redirect('welcome');
        }
        $user_id = Auth::instance()->get_user();
        $activity_id = $this->request->query('id');

		//$usersanswer_count = ORM::factory('usersanswer')->where('user_id', '=', $user_id)->where('activity_id', '=', $activity_id)->count_all();
/////////////////////////////////
		$group = ORM::factory('groups')
			->where('activity_id','=', $activity_id)
			->where('type', '=', '4')
			->find();
/////////////////////////////////
		$id = $this->request->query('id');
		// if($usersanswer_count>0){
		// 	$activity = ORM::factory('activity')->where('activity_id', '=', $id)->find();
		// 	//$this->template->content = View::factory('activity/alreadyviewed')->set('activity', $activity);
		// 	$this->redirect('activity/result?id='.$id);
		// }else{
			$activity = ORM::factory('drawing')->where('id', '=', $id)->find();
			$this->template->content = View::factory('draw')
			->set('activity', $activity)
			->set('group', $group);
		//}

	}



} // End Welcome
