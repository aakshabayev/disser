$(function(){

    // This demo depends on the canvas element
    if(!('getContext' in document.createElement('canvas'))){
        alert('Sorry, it looks like your browser does not support canvas!');
        return false;
    }

    // The URL of your web server (the port is set in app.js)

    var doc = $(document),
        win = $(window),
        canvas = $('#paper'),
        ctx = canvas[0].getContext('2d'),
        instructions = $('#instructions');

    // Generate an unique ID
    var id = Math.round($.now()*Math.random());

    // A flag for drawing activity
    var drawing = false;

    var clients = {};
    var cursors = {};

// start websocket
    var activity_id = "1";
    var group_id = "1";
    var user_id = "<?=$user = Auth::instance()->get_user()?>";
    user_id = parseInt(user_id);
    console.log(user_id);

    var Server;

    function send( text ) {
      Server.send( 'message', text );
    }

    console.log('Connecting...');
    // Server = new MyWebSocket('ws://46.101.26.127:9300');
    Server = new MyWebSocket('ws://127.0.0.1:9300');

    //Let the user know we're connected
    Server.bind('open', function() {
        send("page:draw,activity_id:" + activity_id + ",group:" + group_id + ",message:start");
        console.log( "Connected." );
    });

    //OH NOES! Disconnection occurred.
    Server.bind('close', function( data ) {
        console.log( "Disconnected." );
    });

    //Log any messages sent from server
    Server.bind('message', function( payload ) {
        var coordinates = payload.split(',');
        var x = coordinates[0];
        var y = coordinates[1];
        var user_id = coordinates[2];
        console.log(payload)
        // console.log(x);
        // console.log(y);
        // console.log(user_id)
        drawLine(clients[user_id][0], clients[user_id][1], x, y);

        clients[user_id][0] = x;
        clients[user_id][1] = y;
        clients[user_id].updated = $.now();
    });

    Server.connect();
// end websocket

    var prev = {};

    canvas.on('mousedown',function(e){
        e.preventDefault();
        drawing = true;
        prev.x = e.pageX;
        prev.y = e.pageY;

        // Hide the instructions
        instructions.fadeOut();
    });

    doc.bind('mouseup mouseleave',function(){
        drawing = false;
    });

    var lastEmit = $.now();

    doc.on('mousemove',function(e){
        if($.now() - lastEmit > 30 && drawing == true){
            console.log("console = page:draw,activity_id:" + activity_id + ",group:" + group_id + ",message:" + e.pageX + "," + e.pageY);
            send("page:draw,activity_id:" + activity_id + ",group:" + group_id + ",message:" + e.pageX + "," + e.pageY + "," + user_id)
            lastEmit = $.now();
        }

        // Draw a line for the current user's movement, as it is
        // not received in the socket.on('moving') event above

        if(drawing){

            drawLine(prev.x, prev.y, e.pageX, e.pageY);

            prev.x = e.pageX;
            prev.y = e.pageY;
        }
    });

    // Remove inactive clients after 10 seconds of inactivity
    setInterval(function(){

        for(ident in clients){
            if($.now() - clients[ident].updated > 10000){

                // Last update was more than 10 seconds ago.
                // This user has probably closed the page

                cursors[ident].remove();
                delete clients[ident];
                delete cursors[ident];
            }
        }

    },10000);

    function drawLine(fromx, fromy, tox, toy){
        ctx.moveTo(fromx, fromy);
        ctx.lineTo(tox, toy);
        ctx.stroke();
    }

});