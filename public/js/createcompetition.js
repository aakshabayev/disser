n = 0;
m = -1; //row
l = 0;
function addType() {
      var x = document.getElementById("mySelect").selectedIndex;
      
      if(x==0){
            l = 1;
            n = n +1;
            m = m +1; //row
            var table = document.getElementById("question_type_table");
            var row = table.insertRow(m);
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            cell2.className = 'com_td';
    
            // cell1.className = 'reg_td';
            // cell2.className = 'reg_td';
            cell1.innerHTML = '<p align="center"><b> Question # '+n+'</b></p>';
            cell2.innerHTML =' <input name="type_'+n+'" type="hidden" value="multiple"><input name="question_'+n+'" type="text" class="question" placeholder="question_'+n+'" title="" style="background:yellow;" required><input name="answer_'+n+'_1" type="text" class="special"  placeholder="answer_'+n+'_1" title="" required><input name="answer_'+n+'" type="radio" required value="1"><input name="answer_'+n+'_2" type="text" class="special" placeholder="answer_'+n+'_2" title="" required><input name="answer_'+n+'" type="radio" value="2"><input name="answer_'+n+'_3" type="text" class="special" placeholder="answer_'+n+'_3" title="" required><input name="answer_'+n+'" type="radio" value="3"><input name="answer_'+n+'_4" type="text" class="special" placeholder="answer_'+n+'_4" title="" required><input name="answer_'+n+'" type="radio" value="4"><input name="answer_'+n+'_5" type="text" class="special" placeholder="answer_'+n+'_5" title="" required><input name="answer_'+n+'" type="radio" value="5">';
            // TEXTAREA.name = "question" + k;
            if ( m < 0) {
                  document.getElementById("del_more_question").disabled = true;  
                  document.getElementById("del_more_question").style.background = "#d3d3d3";                 
                  document.getElementById("complete").disabled = true;      
                  document.getElementById("complete").style.background = "#d3d3d3";
            }else{
                  document.getElementById("del_more_question").disabled = false;      
                  document.getElementById("del_more_question").style.background = "red"; 
                  document.getElementById("complete").disabled = false;      
                  document.getElementById("complete").style.background = "#000"; 
            };
      };

      if(x==1){
            l = 2;
            n = n +1;
            m = m +1; //row
            var table = document.getElementById("question_type_table");
            var row = table.insertRow(m);
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
    
            //cell1.className = 'reg_td';
            cell2.className = 'com_td';
            cell1.innerHTML = '<p align="center"><b> Question # '+n+'</b></p>';
            cell2.innerHTML ='<input name="type_'+n+'" type="hidden" value="open"><input name="question_'+n+'" type="text" class="special"  placeholder="question_'+n+'" title="" style="background:yellow;" required><input name="answer_'+n+'_1" type="text" class="special" placeholder="answer_'+n+'_1" title="" required><input name="answer_'+n+'_2" type="text" class="special" placeholder="answer_'+n+'_2" title="" required><input name="answer_'+n+'_3" type="text" class="special" placeholder="answer_'+n+'_3 optional" title=""><input name="answer_'+n+'_4" type="text" class="special" placeholder="answer_'+n+'_4 optional" title=""><input name="answer_'+n+'_5" type="text" class="special" placeholder="answer_'+n+'_5 optional" title="">';
            // TEXTAREA.name = "question" + k;
            if ( m < 0) {
                  document.getElementById("del_more_question").disabled = true;  
                  document.getElementById("del_more_question").style.background = "#d3d3d3";                 
                  document.getElementById("complete").disabled = true;      
                  document.getElementById("complete").style.background = "#d3d3d3";
            }else{
                  document.getElementById("del_more_question").disabled = false;      
                  document.getElementById("del_more_question").style.background = "red"; 
                  document.getElementById("complete").disabled = false;      
                  document.getElementById("complete").style.background = "#000";
            };
      };



      if (x==2) {
            l = 3;
            n = n +1;
            m = m +1; //row
            var table = document.getElementById("question_type_table");
            var row = table.insertRow(m);
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            cell2.className = 'com_td';
            // cell1.className = 'reg_td';
            // cell2.className = 'reg_td';
            cell1.innerHTML = '<p align="center"><b> Question # '+n+' for YES/NO answer</b></p>';
            cell2.innerHTML ='<input name="type_'+n+'" type="hidden" value="yesno"><input name="question_'+n+'" type="text" class="special" placeholder="question_'+n+'" title="" style="background:yellow;" required><br>YES <input name="answer_'+n+'" type="radio" required value="1"><br>NO <input name="answer_'+n+'" type="radio" value="0">';
            // TEXTAREA.name = "question" + k;
            if ( m < 0) {
                  document.getElementById("del_more_question").disabled = true;  
                  document.getElementById("del_more_question").style.background = "#d3d3d3";                 
                  document.getElementById("complete").disabled = true;      
                  document.getElementById("complete").style.background = "#d3d3d3";
            }else{
                  document.getElementById("del_more_question").disabled = false;      
                  document.getElementById("del_more_question").style.background = "red"; 
                  document.getElementById("complete").disabled = false;      
                  document.getElementById("complete").style.background = "#000";
            };
      };
};



function delType() {
      
      if (l==1){
            n = n - 1;
            document.getElementById("question_type_table").deleteRow(m);
            m = m - 1;
            if ( m < 0) {
                  document.getElementById("del_more_question").disabled = true; 
                  document.getElementById("del_more_question").style.background = "#d3d3d3"; 
                  document.getElementById("complete").disabled = true;      
                  document.getElementById("complete").style.background = "#d3d3d3";
            }else{
                  document.getElementById("del_more_question").disabled = false;      
                  document.getElementById("del_more_question").style.background = "red";
                  document.getElementById("complete").disabled = false;      
                  document.getElementById("complete").style.background = "#000"; 
            };
      };

      if (l==2){
            n = n - 1;
            document.getElementById("question_type_table").deleteRow(m);
            m = m - 1;
            if ( m < 0) {
                  document.getElementById("del_more_question").disabled = true; 
                  document.getElementById("del_more_question").style.background = "#d3d3d3"; 
                  document.getElementById("complete").disabled = true;      
                  document.getElementById("complete").style.background = "#d3d3d3";
            }else{
                  document.getElementById("del_more_question").disabled = false;      
                  document.getElementById("del_more_question").style.background = "red"; 
                  document.getElementById("complete").disabled = false;      
                  document.getElementById("complete").style.background = "#000";
            };
      };

      if (l==3){
            n = n - 1;
            document.getElementById("question_type_table").deleteRow(m);
            m = m - 1;
            if ( m < 0) {
                  document.getElementById("del_more_question").disabled = true; 
                  document.getElementById("del_more_question").style.background = "#d3d3d3"; 
                  document.getElementById("complete").disabled = true;      
                  document.getElementById("complete").style.background = "#d3d3d3";
            }else{
                  document.getElementById("del_more_question").disabled = false;      
                  document.getElementById("del_more_question").style.background = "red"; 
                  document.getElementById("complete").disabled = false;      
                  document.getElementById("complete").style.background = "#000";
            };
      };
};
  
